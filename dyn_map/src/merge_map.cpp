/**
  * Node merge different occupancy grids into a Dynamic Occupancy Grid
  * @author Fabio Brea
  * @version 1.0
  */

#ifndef MERGE_MAP_CPP
#define MERGE_MAP_CPP

#include <ros/ros.h>
#include <ros/console.h>
#include <cmath>
#include <dyn_msgs/Obstacle.h>
#include <dyn_msgs/DynamicOccupancyGrid.h>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Empty.h>
#include <dyn_utilities/System.hpp>
#include <dyn_utilities/getPose.h>
#include <geometry_msgs/Pose.h>
#include <tf/tf.h>

/**
 * @brief The Map class is a wrapper class for an occupancy grid with its id
 */
class Map
{

private:

    std::string id;
    std::vector<int8_t> map;
    geometry_msgs::Pose pose;

public:

    /**
     * @brief Map: Constructor for the class
     * @param id
     * @param map
     * @param pose
     */
    Map(std::string id, std::vector<int8_t> map, geometry_msgs::Pose pose)
    {
        this->id = id;
        this->map = map;
        this->pose = pose;
    }

    /**
     * @brief getId gets the id of the map
     * @return the id
     */
    std::string getId()
    {
        return id;
    }

    /**
     * @brief getMap gets the map
     * @return the map
     */
    std::vector<int8_t> getMap()
    {
        return map;
    }

    /**
     * @brief getPose gets the position of the robot at the time the map was acquired
     * @return the pose
     */
    geometry_msgs::Pose getPose()
    {
        return pose;
    }

};

/**
 * @brief The MapPublisher class keeps information on the sensors that publish on the merge topic
 */
class MapPublisher
{

private:

    std::string id;
    std::vector<double> ranges;
    std::vector<double> costs;

public:

    /**
     * @brief MapPublisher: Constructor for the class
     * @param id: the id of the publisher
     * @param ranges: the ranges at which the sensor has different costs
     * @param costs: the costs at the corresponding range
     */
    MapPublisher(std::string id, std::vector<double> ranges, std::vector<double> costs)
    {
        this->id = id;
        this->ranges = ranges;
        this->costs = costs;
    }

    /**
     * @brief getId gets the id of the publisher
     * @return the id
     */
    std::string getId()
    {
        return id;
    }

    /**
     * @brief getCost gets the cost of the publisher at the distance in input
     * @param distance: the distance at which calculate the cost
     * @return the cost
     */
    double getCost(double distance)
    {
        int index = getIndex(distance);

        if(index == costs.size()) //Research in getIndex returned a range > max
            return 0;
        else
            return costs.at(index);
    }

private:

    /**
     * @brief getIndex returns the index at which we are calculating the cost, costs.size if not found
     * @param distance: the distance at which calculate the cost
     * @return the index of the corresponding cost
     */
    int getIndex(double distance)
    {
        int index = 0;
        for(std::vector<double>::iterator i = ranges.begin(); i != ranges.end(); ++i)
        {
            double range = *i;

            if(distance < range)
            {
                return index;
            }

            index++;
        }

        //Index should be == ranges.size() here
        return index;
    }
};

class Merger
{

private:

    ros::NodeHandle nh;
    ros::NodeHandle ph;
    ros::Publisher obs_topic;
    ros::Publisher sensor_topic;
    ros::ServiceClient pose_client;
    System robot;

    std::vector<Map> acquired_map;
    std::vector<MapPublisher> publishers;
    int obstacle_number;

    std::string fusion_frame_id;
    float robot_diameter;
    int num_sensor;

    int width;
    int height;
    float resolution;

    int robot_x;
    int robot_y;

    float strength_param;
    float decay_param;
    float weight_param;

    float security_range;
    float security_angle;

public:

    /**
     * @brief Merger: Constructor for the object
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle
     * @param obs_topic: topic on which publish the merged map
     * @param sensor_topic: topic on which we can find active sensors
     * @param pose_client: can call the service to get the current pose
     */
    Merger(ros::NodeHandle &nh, ros::NodeHandle &ph, ros::Publisher &obs_topic, ros::Publisher &sensor_topic, ros::ServiceClient pose_client)
    {
        this->nh = nh;
        this->ph = ph;
        this->obs_topic = obs_topic;
        this->sensor_topic = sensor_topic;
        this->pose_client = pose_client;

        ph.getParam("num_sensor", num_sensor);
        ph.getParam("fusion_frame_id", fusion_frame_id);
        ph.getParam("width", width);
        ph.getParam("height", height);
        ph.getParam("resolution", resolution);
        ph.getParam("robot_diameter", robot_diameter);
        ph.getParam("strength_param", strength_param);
        ph.getParam("decay_param", decay_param);
        ph.getParam("weight_param", weight_param);
        ph.getParam("security_range", security_range);
        ph.getParam("security_angle", security_angle);

        this->robot_x = std::floor(width / 2.00f);
        this->robot_y = std::floor(height / 2.00f);

        //Load range and cost of all possible publisher of the occupancy grid
        int i = 0;
        while(true)
        {
            std::string id;
            std::vector<double> ranges, costs;

            //Convert number to string
            std::stringstream num_to_str;
            num_to_str << i;

            std::string current_publisher = "publisher" + num_to_str.str() + "/";

            ph.getParam(current_publisher + "id", id);
            ph.getParam(current_publisher + "ranges", ranges);
            ph.getParam(current_publisher + "costs", costs);

            //Stopping condition
            if(id == "")
                break;

            MapPublisher tmp_publisher(id, ranges, costs);
            publishers.push_back(tmp_publisher);
            ++i;
        }

        obstacle_number = 0;

        robot.setStatus(READY);
    }

    /**
     * @brief fromOrientationToRPY transform a quaternion in the corrispondent roll, pitch and yaw
     * @param orientation: the orientation to give in input
     * @param roll: the roll given by the orientation
     * @param pitch: the pitch given by the orientation
     * @param yaw: the yaw given by the orientation
     */
    void fromOrientationToRPY(geometry_msgs::Quaternion orientation, double& roll, double& pitch, double& yaw)
    {
        tf::Quaternion tf_orientation;
        tf::quaternionMsgToTF(orientation, tf_orientation);
        tf::Matrix3x3 matrix(tf_orientation);
        matrix.getRPY(roll, pitch, yaw);
    }

    double getYawRotation(geometry_msgs::Quaternion origin, geometry_msgs::Quaternion destination)
    {
        double origin_roll, origin_pitch, origin_yaw;
        double destination_roll, destination_pitch, destination_yaw;
        fromOrientationToRPY(origin, origin_roll, origin_pitch, origin_yaw);
        fromOrientationToRPY(destination, destination_roll, destination_pitch, destination_yaw);

        //Sign correction
        if(destination_yaw * origin_yaw <= 0 && std::fabs(destination_yaw) > M_PI / 2 && std::fabs(origin_yaw) > M_PI / 2)
        {
            int sign = (destination_yaw > 0) - (destination_yaw < 0);
            origin_yaw = sign * M_PI + sign * (sign * M_PI + origin_yaw);
        }

        return destination_yaw - origin_yaw;

    }

    std::vector<int8_t> getCurrentMap(std::vector<int8_t> map_to_transform, geometry_msgs::Pose origin_pose, geometry_msgs::Pose destination_pose)
    {
        //Odometry has x and y reversed with respect to the occupancy grid
        double delta_x = destination_pose.position.x - origin_pose.position.x;
        double delta_y = destination_pose.position.y - origin_pose.position.y;
        double rotation = -getYawRotation(origin_pose.orientation, destination_pose.orientation);

        double x_translation = -(cos(rotation) * delta_x - sin(rotation) * delta_y);
        double y_translation = -(sin(rotation) * delta_x + cos(rotation) * delta_y);

        std::vector<int8_t> grid;
        grid.reserve(width * height);

        //Initialize a map without obstacles
        for (int i = 0; i < width * height; ++i)
            grid.push_back(0);

        for (int i = 0; i < width * height; ++i)
        {
            if(map_to_transform.at(i) > 0)
            {
                //Get the distance of the obstacle
                double obstacle_y = std::floor(i / width);
                double obstacle_x = i - (obstacle_y * width);
                float old_y = - (obstacle_x - robot_x) * resolution;
                float old_x = - (obstacle_y - robot_y) * resolution;

                //Transform keeping into account the new position and orientation
                float current_y = -(old_x * cos(rotation) - old_y * sin(rotation) + x_translation);
                float current_x = -(old_x * sin(rotation) + old_y * cos(rotation) + y_translation);

                //Back to map representation
                float current_obstacle_position_x = robot_x + std::ceil(current_x / resolution);
                float current_obstacle_position_y = robot_y + std::ceil(current_y / resolution);

                if(current_obstacle_position_x >= 0 && current_obstacle_position_x < width && current_obstacle_position_y >= 0 && current_obstacle_position_y < height)
                    grid.at((current_obstacle_position_y * width) + current_obstacle_position_x) = 1;

            }
            else if (map_to_transform.at(i) < 0)
            {
                grid.at(i) = map_to_transform.at(i);
            }
        }
        return grid;
    }

    /**
     * @brief obsCallback is the callback that fuses the occupancy grid from different sensors and create the message for the robot
     * @param msg: the occupancy grid to merge
     */
    void obsCallback(const dyn_msgs::OccupancyGridWithPose::ConstPtr& msg)
    {
        std::string frame_id = msg->grid.header.frame_id;
        int current_status = robot.getStatus();

        //Extract the type of the message arrived
        unsigned first = frame_id.find("/") + 1;
        unsigned last = frame_id.find_last_of("/");
        std::string msg_type = frame_id.substr (first, last - first);
        std::string map_id = frame_id.substr(last + 1);

        if(current_status == RUNNING || current_status == PAUSED || current_status == READY || current_status == COLLIDED)
        {
            if(msg_type != "sensor")
                return;

            //Check if we already acquired a map from this sensor, if so, save the position in the vector
            int position = 0;
            for(std::vector<Map>::iterator i = acquired_map.begin(); i != acquired_map.end(); ++i)
            {
                std::string compare_id = i->getId();
                if (compare_id == map_id)
                    break;

                position++;
            }

            Map map(map_id, msg->grid.data, msg->pose);

            //Add the map from a new sensor or update old one
            if(position == acquired_map.size())
            {
                acquired_map.push_back(map);
            }
            else
            {
                acquired_map.at(position) = map;
            }
        }
        else if(current_status == MANUAL)
        {
            if(msg_type != "event")
                return;

            //Drop all acquired maps and save new one
            acquired_map.clear();

            Map map(map_id, msg->grid.data, msg->pose);
            acquired_map.push_back(map);
        }

        //Update the number of active sensors
        std_msgs::Empty synchro_msg;
        sensor_topic.publish(synchro_msg);
        num_sensor = sensor_topic.getNumSubscribers();

        //ROS_ERROR("Have: %d maps. Need: %d", num_sensor);

        if(((current_status == RUNNING  || current_status == PAUSED || current_status == READY || current_status == COLLIDED) && acquired_map.size() >= num_sensor) || current_status == MANUAL)
        {
            //Acquire current publishers, needed to weight the obstacle
            std::vector<MapPublisher> publishers_acquired;
            for(std::vector<Map>::iterator it = acquired_map.begin(); it != acquired_map.end(); ++it)
            {
                std::string id = it->getId();
                for(std::vector<MapPublisher>::iterator pub = publishers.begin(); pub != publishers.end(); ++pub)
                {
                    if(id == pub->getId())
                    {
                        publishers_acquired.push_back(*pub);
                        break;
                    }
                }
            }

            std::vector<int8_t> merged_map;
            merged_map.reserve(width * height);

            //Get the current transformation
            dyn_utilities::getPose srv;
            pose_client.call(srv);
            geometry_msgs::Pose current_pose = srv.response.current_pose;

            //Initialize a map without obstacles
            for (int i = 0; i < width * height; ++i)
                merged_map.push_back(0);

            //Merge all acquired maps
            for(std::vector<Map>::iterator it = acquired_map.begin(); it != acquired_map.end(); ++it)
            {
                //Rotate the map in the new position before merging
                std::vector<int8_t> tmp_map = getCurrentMap(it->getMap(), it->getPose(), current_pose);

                //Merge the current sensor data with the already merged ones
                for (int i = 0; i < width * height; ++i)
                {
                    if (tmp_map.at(i) != 0 && merged_map.at(i) == 0)
                    {
                        obstacle_number++;

                        //Calculate distance to weight the obstacle accordingly
                        double obstacle_y = std::floor(i / width);
                        double obstacle_x = i - (obstacle_y * width);
                        float x = std::abs(obstacle_x - robot_x) * resolution;
                        float y = std::abs(obstacle_y - robot_y) * resolution;

                        double distance = std::sqrt(std::pow(x, 2) + std::pow(y, 2));

                        double total_cost = 0;
                        double cost = 0;
                        std::string tmp_id = it->getId();
                        for(std::vector<MapPublisher>::iterator pub = publishers_acquired.begin(); pub != publishers_acquired.end(); ++pub)
                        {
                            total_cost += pub->getCost(distance);
                            if(pub->getId() == tmp_id)
                                cost = pub->getCost(distance);
                        }
                        if (total_cost > 0)
                        {
                            merged_map.at(i) += tmp_map.at(i) * (cost / total_cost) * 100;
                        }

                        //This shouldn't happen, but it's just to be sure
                        if (merged_map.at(i) > 100)
                            merged_map.at(i) = 100;
                    }
                }
            }

            //Create message with the merged map and dynamic navigation parameters
            dyn_msgs::DynamicOccupancyGrid grid;

            grid.header.frame_id = fusion_frame_id;
            grid.header.stamp = ros::Time::now();
            grid.info = msg->grid.info;
            grid.info.map_load_time = ros::Time::now();
            grid.obstacle = merged_map;

            dyn_msgs::Obstacle obstacle_message;
            bool near_obstacle = false;

            for (int i = 0; i < width * height; ++i)
            {
                int obstacle_position_y = std::floor(i / width);
                int obstacle_position_x = i - (obstacle_position_y * width);

                //Calculate relative position, putting robot at the center of the map
                int signX;
                int signY;

                if (obstacle_position_x > robot_x)
                    signX = 1;
                else
                    signX = -1;

                if (obstacle_position_y > robot_y)
                    signY = -1;
                else
                    signY = 1;

                float x = std::abs(obstacle_position_x - robot_x) * resolution;
                float y = std::abs(obstacle_position_y - robot_y) * resolution;

                //Calculate parameters for dynamic navigation
                float range = std::sqrt(std::pow(x, 2) + std::pow(y, 2));
                float angle = atan2((signX * x), (signY * y));

                float angle_resolution = tan(1.0f / (width - robot_x - 1));

                double current_strength = strength_param;
                if(signX == 1)
                    current_strength *= 1.1;

                float strength = merged_map.at(i) * current_strength * (1.0f / obstacle_number) * std::exp(- (range-resolution) / decay_param);
                float weight = weight_param * atan2( std::tan(angle_resolution/2) + robot_diameter/(robot_diameter + range), 1);

                //Handling of near obstacles
                if (strength > 0 && angle > - security_angle && angle < security_angle)
                {
                    if (range < security_range)
                        near_obstacle = true;
                }

                obstacle_message.strength = strength;
                obstacle_message.direction = -angle;    //To have the same map/robot heading
                obstacle_message.weight = weight;

                grid.data.push_back(obstacle_message);
            }
            grid.near_obstacle = near_obstacle;

            //Clear acquired maps
            acquired_map.clear();

            //Reset number of obstacle
            obstacle_number = 0;

            //Publish the merged map
            obs_topic.publish(grid);
        }
    }

    /**
      * @brief statusCallback check for change in robot state
      * @param status_msg: the state of the robot
      */
    void statusCallback(const std_msgs::Int32::ConstPtr& msg)
    {
        if(robot.hasChangedStatus(msg->data))
        {
            robot.setStatus(msg->data);
            //ROS_ERROR("[MERGER] Status: %s", robot.getStringStatus((robot.getStatus())).c_str()); //Only to control if we have the same status published
        }
    }

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "merge_map");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    std::string maps_topic, sensor_topic, merged_map_topic, status_topic;
    ph.getParam("maps_topic" , maps_topic);
    ph.getParam("sensor_topic" , sensor_topic);
    ph.getParam("merged_map_topic" , merged_map_topic);
    ph.getParam("robot_status_topic", status_topic);

    //Publisher for the node
    ros::Publisher obstacles_publisher = nh.advertise<dyn_msgs::DynamicOccupancyGrid>(merged_map_topic, 1);
    ros::Publisher sensor_publisher = nh.advertise<std_msgs::Empty>(sensor_topic, 1);
    ros::ServiceClient pose_client = nh.serviceClient<dyn_utilities::getPose>("get_pose");

    //Instantiation of the callback object
    Merger foundry(nh, ph, obstacles_publisher, sensor_publisher, pose_client);

    //Instantiation of the subscribers for the node
    ros::Subscriber occupancy_subscriber = nh.subscribe<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1, &Merger::obsCallback, &foundry);
    ros::Subscriber sub_status = nh.subscribe<std_msgs::Int32>(status_topic, 1, &Merger::statusCallback, &foundry);

    ros::AsyncSpinner spinner(0);
    spinner.start();

    ros::spin();

    return 0;
}

#endif
