/**
  * Node to draw a Dynamic Occupancy Grid
  * @author Brea Fabio
  * @version 1.0
  */

#ifndef DRAW_CPP
#define DRAW_CPP

#include <ros/ros.h>
#include <sstream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <dyn_msgs/DynamicOccupancyGrid.h>

//Define colors that will be used in the map
const cv::Vec3b BLACK = cv::Vec3b(0, 0, 0);
const cv::Vec3b WHITE = cv::Vec3b(255, 255, 255);
const cv::Vec3b GREEN = cv::Vec3b(0, 255, 0);
const cv::Vec3b RED = cv::Vec3b(0, 0, 255);
const cv::Vec3b DARKORANGE = cv::Vec3b(0, 128, 255);
const cv::Vec3b ORANGE = cv::Vec3b(0, 165, 255);
const cv::Vec3b YELLOW = cv::Vec3b(0, 255, 255);
const cv::Vec3b LIGHTBLUE = cv::Vec3b(165, 103, 0);
const cv::Vec3b CYAN = cv::Vec3b(187, 57, 28);
const cv::Vec3b DARKBLUE = cv::Vec3b(122, 18, 50);
const cv::Vec3b ROSE = cv::Vec3b(127, 0, 255);

class Draw
{

private:

    ros::NodeHandle ph;

    //map variables
    cv::Mat img;
    float width;
    float height;
    int numCellX;
    int numCellY;
    int cellDimension;
    int robot_x;
    int robot_y;

public:

    /**
     * @brief Constructor for the draw node
     * @param ph: a private nodehandle for the node
     */
    Draw(ros::NodeHandle ph)
    {
        this->ph = ph;

        ph.getParam("width", numCellX);
        ph.getParam("height", numCellY);
        ph.getParam("cellDimension", cellDimension);

        this->robot_x = std::floor(numCellX / 2.00f);
        this->robot_y = std::floor(numCellY / 2.00f);

        this->width = numCellX * cellDimension;
        this->height = numCellY * cellDimension;

        this->img = cv::Mat(height, width, CV_8UC3);
    }

    /**
     * @brief drawCallBack is the callback that will draw the occupancy grid through openCV
     * @param msg: the dynamic occupancy grid to draw
     */
    void drawCallBack(const dyn_msgs::DynamicOccupancyGrid::ConstPtr& msg)
    {
        //Reset the map
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                img.at<cv::Vec3b>(j,i) = WHITE;
            }
        }

        float max_influence = 0.00f;
        float min_influence = 100.00f;

        //Calculate the influence of each obstacle and save min and max values
        std::vector<float> obs;
        for (float i = 0; i < numCellX * numCellY; ++i)
        {
            if (msg->obstacle.at(i) > 0)
            {
                dyn_msgs::Obstacle obs_message;
                obs_message = msg->data.at(i);

                float exp_param = - std::pow(obs_message.direction/(obs_message.weight), 2) / 2;
                float obstacle = obs_message.strength * std::exp(exp_param);

                obs.push_back(obstacle);

                if (obstacle > max_influence)
                {
                    max_influence = obstacle;
                }
                if (obstacle < min_influence)
                {
                    min_influence = obstacle;
                }
            }
            else
            {
                obs.push_back(0);
            }
        }

        //Draw on the map
        for (float i = 0; i < numCellX * numCellY; ++i)
        {
            if (msg->obstacle.at(i) > 0) //Draws a repulsor
            {
                int y = std::floor(i / numCellY);
                int x = i - y * numCellY;

                for (int j = y * cellDimension; j < (y + 1) * cellDimension; ++j)
                {
                    for (int k = x * cellDimension; k < (x + 1) * cellDimension; ++k)
                    {
                        if ((obs.at(i)) / max_influence > 0.75f)
                        {
                            img.at<cv::Vec3b>(j,k) = RED;
                        }
                        else if ((obs.at(i))/max_influence > 0.50f)
                        {
                            img.at<cv::Vec3b>(j,k) = DARKORANGE;
                        }
                        else if ((obs.at(i))/max_influence > 0.25f)
                        {
                            img.at<cv::Vec3b>(j,k) = ORANGE;
                        }
                        else if ((obs.at(i))/max_influence > 0.10f)
                        {
                            img.at<cv::Vec3b>(j,k) = YELLOW;
                        }
                        else if ((obs.at(i))/max_influence > 0.01f)
                        {
                            img.at<cv::Vec3b>(j,k) = LIGHTBLUE;
                        }
                        else if ((obs.at(i))/max_influence > 0.001f)
                        {
                            img.at<cv::Vec3b>(j,k) = CYAN;
                        }
                        else
                        {
                            img.at<cv::Vec3b>(j,k) = DARKBLUE;
                        }
                    }
                }
            }
            else if (msg->obstacle.at(i) < 0) //Draws an attractor
            {
                int y = std::floor(i / numCellY);
                int x = i - y * numCellY;

                for (int j = y * cellDimension; j < (y + 1) * cellDimension; ++j)
                {
                    for (int k = x * cellDimension; k < (x + 1) * cellDimension; ++k)
                    {
                        img.at<cv::Vec3b>(j,k) = GREEN;
                    }
                }
            }
        }
        obs.clear();

        //Draws the robot
        for(int i = -1; i < 2 * cellDimension; ++i)
        {
            for(int j = -1; j < 2 * cellDimension; ++j)
            {
                img.at<cv::Vec3b>(robot_y * cellDimension + i,robot_y * cellDimension + j) = ROSE;
            }
        }

        cv::imshow("Robot's field of view", img);
        cv::moveWindow("Robot's field of view", 0, 0);
        cv::waitKey(10);
    }

};


int main(int argc,char** argv)
{
    ros::init(argc, argv, "DrawGrid");

    //The main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    //Initialization of the map object
    Draw map(ph);

    std::string merged_map_topic;
    ph.getParam("merged_map_topic", merged_map_topic);

    //Subscribers of the node
    ros::Subscriber occupancy_subscriber = nh.subscribe<dyn_msgs::DynamicOccupancyGrid>(merged_map_topic, 1, &Draw::drawCallBack, &map);

    ros::spin();
}

#endif
