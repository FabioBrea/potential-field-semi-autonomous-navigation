#include <dyn_utilities/getPose.hpp>

OdomCalculator::OdomCalculator(ros::NodeHandle nh)
{
    this->nh = nh;

    current_pose.position.x = 0;
    current_pose.position.y = 0;
    current_pose.position.z = 0;
    current_pose.orientation.w = 1;
    current_pose.orientation.x = 0;
    current_pose.orientation.y = 0;
    current_pose.orientation.z = 0;
}

bool OdomCalculator::send_pose(dyn_utilities::getPose::Request &req, dyn_utilities::getPose::Response &res)
{
    res.current_pose = current_pose;
    return true;
}

void OdomCalculator::odomCallback(const nav_msgs::Odometry::ConstPtr& odometry_msg)
{
    current_pose = odometry_msg->pose.pose;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pose_server");

    ros::NodeHandle nh;
    OdomCalculator calc(nh);
    ros::Subscriber sub_odom = nh.subscribe<nav_msgs::Odometry>("/odom", 1, &OdomCalculator::odomCallback , &calc);
    ros::ServiceServer service = nh.advertiseService("get_pose", &OdomCalculator::send_pose, &calc);

    ros::spin();
    return 0;
}
