/**
  * Implementation of the Status class, that continuously publish the current status of the robot
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_utilities/Status.hpp>

#ifndef STATUS_CPP
#define STATUS_CPP


/**
 * @brief Contructor for the class
 * @param nh: the main nodehandle
 * @param pub: the topic on which the status will be published
 */
Status::Status(ros::NodeHandle nh, ros::Publisher pub)
{
    this->nh = nh;
    this->status_pub = pub;

    robot.setStatus(PAUSED);
}


/**
  * @brief statusCallback is the callback that change the robot status if a new status arrives in input
  * @param status_msg: the published status
  */
void Status::statusCallback(const std_msgs::Int32::ConstPtr& status_msg)
{
    if(robot.hasChangedStatus(status_msg->data))
    {
        robot.setStatus(status_msg->data);
    }
}


/**
 * @brief status_publisher continuously publish the current status of the robot
 */
void Status::status_publisher()
{
    ros::Rate loop_rate(10);
    while(ros::ok())
    {
        std_msgs::Int32 actual_status;
        actual_status.data = robot.getStatus();
        status_pub.publish(actual_status);
        loop_rate.sleep();
        ros::spinOnce();
    }
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "Robot_Status");

    //Main nodehandle
    ros::NodeHandle nh;
    ros::NodeHandle ph("~");

    std::string status_topic, change_status_topic;
    ph.getParam("robot_status_topic", status_topic);
    ph.getParam("robot_change_status_topic", change_status_topic);

    //Topic on which status has to be published
    ros::Publisher pub_obstacle = nh.advertise<std_msgs::Int32>(status_topic, 1);

    Status status_observer(nh, pub_obstacle);

    ros::Subscriber sub_status = nh.subscribe<std_msgs::Int32>(change_status_topic, 1, &Status::statusCallback, &status_observer);

    //Start publishing status
    status_observer.status_publisher();

    ros::spin();

    return 0;
}

#endif
