/**
  * Implementation of a System class, that creates a DFA
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_utilities/System.hpp>

#ifndef SYSTEM_CPP
#define SYSTEM_CPP

/**
 * @brief System initialize the status of the system to PAUSED
 */
System::System()
{
    status = PAUSED;
}

/**
 * @brief System initialize the status of the system with the input status
 * @param status: the status of the system
 */
System::System(int status)
{
    this->status = status;
}

/**
 * @brief getStatus get the current status of the system
 * @return the status of the system
 */
int System::getStatus()
{
    return status;
}

/**
 * @brief setStatus set a status for the system
 * @param status: the status to be setted
 */
void System::setStatus(int status)
{
    this->status = status;
}

/**
 * @brief hasChangedStatus return true if the system has changed status
 * @param new_status the new status of the system
 * @return true if the status of the system has changed
 */
bool System::hasChangedStatus(int new_status)
{
    return (status != new_status);
}

/**
 * @brief getStringStatus return the translation from a status to a string
 * @param status: the status that will be translated
 * @return a string, the status translated
 */
std::string System::getStringStatus(int status)
{
    if(status == READY)
        return "READY";
    else if(status == RUNNING)
        return "RUNNING";
    else if(status == PAUSED)
        return "PAUSED";
    else if(status == STOPPED)
        return "STOPPED";
    else if(status == MANUAL)
        return "MANUAL CONTROL";
    else if(status == COLLIDED)
        return "COLLIDED";
}

#endif
