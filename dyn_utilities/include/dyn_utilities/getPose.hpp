/**
  * Header for the getPose service class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <dyn_utilities/getPose.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <ros/console.h>

#ifndef GETPOSE_HPP
#define GETPOSE_HPP

class OdomCalculator
{

private:

    ros::NodeHandle nh;
    geometry_msgs::Pose current_pose;

public:

    /**
     * @brief Contructor for the class
     * @param nh: the main nodehandle
     */
    OdomCalculator(ros::NodeHandle nh);

public:


	bool send_pose(dyn_utilities::getPose::Request  &req, dyn_utilities::getPose::Response &res);

    /**
      * @brief statusCallback is the callback that change the robot status if a new status arrives in input
      * @param status_msg: the published status
      */
    void odomCallback(const nav_msgs::Odometry::ConstPtr& odometry_msg);


};

#endif
