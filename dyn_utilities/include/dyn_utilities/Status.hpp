/**
  * Header for the Status class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <dyn_utilities/System.hpp>

#ifndef STATUS_HPP
#define STATUS_HPP

class Status
{

private:

    ros::NodeHandle nh;
    ros::Publisher status_pub;
    System robot;

public:

    /**
     * @brief Contructor for the class
     * @param nh: the main nodehandle
     * @param pub: the topic on which the status will be published
     */
    Status(ros::NodeHandle nh, ros::Publisher pub);

public:

    /**
      * @brief statusCallback is the callback that change the robot status if a new status arrives in input
      * @param status_msg: the published status
      */
    void statusCallback(const std_msgs::Int32::ConstPtr& status_msg);

    /**
     * @brief status_publisher continuously publish the current status of the robot
     */
    void status_publisher();

};

#endif
