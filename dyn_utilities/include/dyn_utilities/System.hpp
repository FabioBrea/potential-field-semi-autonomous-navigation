/**
  * Header for the System class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>

#ifndef SYSTEM_HPP
#define SYSTEM_HPP

const int READY    = 0;
const int RUNNING  = 1;
const int PAUSED   = 2;
const int STOPPED  = 3;
const int MANUAL   = 4;
const int COLLIDED = 5;

class System
{

private:

    int status;

public:

    /**
     * @brief System initialize the status of the system to PAUSED
     */
    System();

    /**
     * @brief System initialize the status of the system with the input status
     * @param status: the status of the system
     */
    System(int status);

public:

    /**
     * @brief getStatus get the current status of the system
     * @return the status of the system
     */
    int getStatus();

    /**
     * @brief setStatus set a status for the system
     * @param status: the status to be setted
     */
    void setStatus(int status);

    /**
     * @brief hasChangedStatus return true if the system has changed his status
     * @param new_status: the new status of the system
     * @return true if the status of the system has changed
     */
    bool hasChangedStatus(int new_status);

    /**
     * @brief getStringStatus return the translation from a status to a string
     * @param status: the status that will be translated
     * @return a string, the status translated
     */
    std::string getStringStatus(int status);
};

#endif
