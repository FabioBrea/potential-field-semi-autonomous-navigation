**Ros packages required**:

- ROS indigo

- Orobot (https://bitbucket.org/iaslab-unipd/orobot)

- pointcloud_to_laserscan

- iai_kinect2

**External libraries required**:

- Gstreamer1.0 (only for telepresence, if you cannot or do not want to download, remove video_streaming package, the system works even without it)

**dyn_utilities**

Contains those utilities classes that allow the creation of a DFA and a service that allow to get the current position of the robot, with respect to the initial position.

`roslaunch dyn_utilities system.launch`

This package also contains a launcher that can be used to execute the entire system at once.

`roslaunch dyn_utilities unified_launcher.launch`

**dyn_event**

Contains those classes that allow to use those sensors that do not stream continuously their status, but give information when an event occur. We have implemented bumpers and keyboard.

`roslaunch dyn_event event.launch`

With keyboard you can give these commands:

- r : ready, the robot will start moving after 5 seconds from the keyboard stroke

- p : paused, put the system in pause, but the dynamic map is still published

- q : stopped, the robot has arrived at the goal and the node is arrested

- Arrow keys (left and right) : make the robot turn in that direction

With default settings, keyboard commands can be given only from the robot keyboard, if you want to give command remotely, edit file params/Keyboard_settings.yaml using REMOTE as connection_type. Then, from the remote laptop:

`rosrun dyn_event sender "ip" "port"`

where ip and port are the robot's.

**dyn_map**

Contains those classes that allow to merge maps from different sensors/events and to visualize the merged map. It also contains configuration files in params folder, that must be edit when new sensors are implemented or you want to change the cost of each sensor at a given distance.

`roslaunch dyn_map merger.launch`

**dyn_navigation**

Contains those classes that allow the robot to move. Navigation translate the potential field in velocity, while Controller allow to move the robot depending on the system status. In launcher file must be edit the topic name if a TurtleBot is not used.

`roslaunch dyn_navigation navigation.launch`

**dyn_sensor**

Contains those classes that translate sensor data into occupancy grids and the class that allow to calculate the transformation between two laser flows, given pair of images of the flows in the folder images. In the params folder, the Kinect2_settings.yaml file contains the parameters used to filer the point cloud, while TransformationParameters.yaml have the calculated transformation saved in. The class that calculate the transformation can be executed with:
`roslaunch dyn_sensor calibrateLaserScan.launch`

Laser sensors and Kinect can be launcher through:

`roslaunch dyn_sensor laser.launch`

`roslaunch dyn_sensor kinect.launch`

Per lanciare assieme tutti i sensori disponibili:

`roslaunch dyn_sensor sensor.launch`

**dyn_test**

Contains a test class that simulate a sensor, if you want to use it, do not launch the system through the unified launcher or do not execute the dyn_sensor package.

`roslaunch dyn_test test.launch`

**video_streaming**

Works after installing GStreamer1.0 and its plugins, this package contains those classes that allow to stream audio, using laptop microphone, and video, using webcam and/or Kinect v2. From launcher you can set which nodes to launch and set the ip addresses and ports necessary to stream.

`roslaunch video_streaming video_streaming.launch`