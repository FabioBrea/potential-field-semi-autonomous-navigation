/**
  * The navigation class takes dynamic occupancy grids in input and output a new velocity, depending on the potential field
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <cmath>
#include <dyn_msgs/Obstacle.h>
#include <dyn_msgs/DynamicOccupancyGrid.h>
#include <geometry_msgs/Twist.h>

#ifndef NAVIGATION_CPP
#define NAVIGATION_CPP

class Navigation
{

private:

    ros::NodeHandle nh;
    ros::Publisher vel_topic;

    double max_velocity;

public:

    /**
     * @brief Constructor of the class
     * @param nh: the nodehandle for the class
     * @param vel_publisher: publisher of the velocity controller topic
     * @param max_velocity: the maximum linear velocity for the robot
     */
    Navigation(ros::NodeHandle &nh, ros::Publisher &vel_publisher, double max_velocity)
    {
        this->nh = nh;
        this->vel_topic = vel_publisher;
        this->max_velocity = max_velocity;
    }

    /**
     * @brief velCallback is a callback that calculate the next direction and velocity for the robot
     * @param msg: the dynamic occupancy grid of merged data
     */
    void velCallback(const dyn_msgs::DynamicOccupancyGrid::ConstPtr& msg)
    {
        std::vector<dyn_msgs::Obstacle> data = msg->data;

        geometry_msgs::Twist new_vel;
        float new_direction = 0.00f;

        //Calculation of the new direction, based on the actual potential field
        for(std::vector<dyn_msgs::Obstacle>::iterator it = data.begin(); it != data.end(); ++it)
        {
            dyn_msgs::Obstacle obstacle = *it;
            float exp_param = - std::pow(obstacle.direction / obstacle.weight, 2) / 2;
            new_direction += - obstacle.strength * obstacle.direction * std::exp(exp_param);
        }

        new_vel.angular.z = new_direction;

        //Calculation of the new velocity, based on the distance from the obstacles or on the new direction
        if (msg->near_obstacle)
        {
            new_vel.linear.x = 0.00f;
            //ROS_WARN("[Navigation] Obstacles near, robot is only allowed to turn on the place.");
        }
        else
        {
            new_vel.linear.x = max_velocity * std::cos(new_direction);
        }

        vel_topic.publish(new_vel);
    }

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "Navigation");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle to load parameters
    ros::NodeHandle ph("~");

    //Loading parameters
    std::string merged_map_topic, speed_controller_topic;
    double max_velocity = 0.00f;
    ph.getParam("merged_map_topic", merged_map_topic);
    ph.getParam("speed_controller_topic", speed_controller_topic);
    ph.getParam("max_velocity", max_velocity);

    //Publisher for the node
    ros::Publisher velocity = nh.advertise<geometry_msgs::Twist>(speed_controller_topic, 1);

    //Instantiation of the callback object
    Navigation navigation(nh, velocity, max_velocity);

    //Instantiation of the subscribers for the node
    ros::Subscriber sub_grid = nh.subscribe<dyn_msgs::DynamicOccupancyGrid>(merged_map_topic, 1, &Navigation::velCallback, &navigation);

    ros::spin();

    return 0;
}

#endif
