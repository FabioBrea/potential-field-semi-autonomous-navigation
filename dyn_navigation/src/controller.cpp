/**
  * The controller class uses arriving velocity message and, depending on the robot status,
  * manages the robot velocity
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <deque>
#include <std_msgs/Int32.h>
#include <dyn_utilities/System.hpp>

#ifndef CONTROLLER_CPP
#define CONTROLLER_CPP

class Controller
{

private:

    ros::NodeHandle nh;
    ros::Publisher vel_topic;
    ros::Publisher status_topic;
    ros::Time time_last_change;

    System robot;
    std::deque<float> last_angles;
    geometry_msgs::Twist vel;
    int ready_timer;
    //    bool turningPause;
    double max_velocity;

public:

    /**
     * @brief Constructor for the object
     * @param nh: the main nodehandle
     * @param vel_publisher: publisher of the new velocity
     * @param status_publisher: publisher of the robot status
     * @param max_velocity: the maximum linear velocity for the robot
     */
    Controller(ros::NodeHandle &nh, ros::Publisher &vel_publisher, ros::Publisher &status_publisher, double max_velocity)
    {
        this->nh = nh;
        this->vel_topic = vel_publisher;
        this->status_topic = status_publisher;
        this->max_velocity = max_velocity;
        //        this->turningPause = false;
        this->ready_timer = 5;
        this->robot.setStatus(PAUSED);
    }

    /**
     * @brief speedCallback is the callback that calculate the next angle for the robot
     * @param msg: the message with a new proposed velocity
     */
    void speedCallback(const geometry_msgs::Twist::ConstPtr& msg)
    {
        //Keeping the same queue size, deleting the oldest message
        if(last_angles.size() == 2)
            last_angles.pop_front();

        last_angles.push_back(msg->angular.z);
        vel = *msg;
    }

    /**
     * @brief speedControl manages the velocity given from the potential field depending on the current status
     */
    void speedControl()
    {
        ros::Rate loop_rate(10);

        //Keeps the robot active
        while(ros::ok())
        {
            int currentStatus = robot.getStatus();

            if(currentStatus == RUNNING)
            {
                float average_angular_speed = 0.00f;

                //Averaging angular velocity
                std::deque<float>::iterator it;
                for(it = last_angles.begin(); it != last_angles.end(); ++it)
                {
                    average_angular_speed += *it;
                }

                average_angular_speed /= last_angles.size();

                vel.angular.z = average_angular_speed;

                if(vel.linear.x != 0)
                    vel.linear.x = max_velocity * std::cos(average_angular_speed);

                vel_topic.publish(vel);
                loop_rate.sleep();

                //                //Stops the robot to synchronize slow topics
                //                if(std::fabs(vel.angular.z) > 0.78)
                //                {
                //                    ros::Time init = ros::Time::now();
                //                    while((ros::Time::now() - init).toSec() < 0.2)
                //                    {
                //                        vel_topic.publish(vel);
                //                        loop_rate.sleep();
                //                    }
                //                    std_msgs::Int32 pause_msg;
                //                    pause_msg.data = PAUSED;
                //                    status_topic.publish(pause_msg);
                //                    turningPause = true;
                //                }
            }
            else if(currentStatus == MANUAL)
            {
                if (last_angles.size() > 0)
                {
                    vel.angular.z = last_angles.back();
                    last_angles.pop_back();
                }

                vel_topic.publish(vel);
                last_angles.clear();
                loop_rate.sleep();
            }
            else if(currentStatus == COLLIDED)
            {
                if((ros::Time::now() - time_last_change).toSec() < 3)
                {
                    vel.linear.x = - max_velocity * 1.2;
                    vel.angular.z = 0;
                    vel_topic.publish(vel);
                    loop_rate.sleep();
                }
                else
                {
                    //Retreat finished, run again, with a new obstacle
                    std_msgs::Int32 run_msgs;
                    run_msgs.data = RUNNING;

                    status_topic.publish(run_msgs);
                    ros::spinOnce();
                }
            }
            else if(currentStatus == PAUSED)
            {
                vel.linear.x = 0;
                vel.angular.z = 0;
                vel_topic.publish(vel);
                ros::Duration(1).sleep();

                //                //This will be used when after topic synchronization
                //                if(turningPause)
                //                {
                //                    turningPause = false;
                //                    std_msgs::Int32 go_msg;
                //                    go_msg.data = RUNNING;
                //                    status_topic.publish(go_msg);
                //                }
            }
            else if(currentStatus == READY)
            {
                if(ready_timer != 0)
                {
                    ROS_INFO("[ROBOT] System will start running in %d seconds", ready_timer);
                    ready_timer--;
                    ros::Duration(1).sleep();
                }
                else
                {
                    std_msgs::Int32 go_msg;
                    go_msg.data = RUNNING;
                    status_topic.publish(go_msg);
                }
            }
            else if(currentStatus == STOPPED)
            {
                vel.linear.x = 0;
                vel.angular.z = 0;
                vel_topic.publish(vel);
                ros::Duration(1).sleep();
                ROS_INFO("[ROBOT] Target reached. Shutting down the system.");
                ros::shutdown();
            }

            ros::spinOnce(); //This will update the queue and robot status through the callback
        }
    }

    /**
      * @brief statusCallback check for change in robot state
      * @param status_msg: the state of the robot
      */
    void statusCallback(const std_msgs::Int32::ConstPtr& msg)
    {
        if(robot.hasChangedStatus(msg->data))
        {
            robot.setStatus(msg->data);
            ROS_ERROR("[ROBOT] Status: %s", robot.getStringStatus((robot.getStatus())).c_str());

            //Reset velocity, waiting for next messages
            vel.linear.x = 0;
            vel.angular.z = 0;
            vel_topic.publish(vel);
            last_angles.clear();
            ready_timer = 5;
            ros::Duration(0.5).sleep();
            time_last_change = ros::Time::now();
        }
    }

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "speed_controller");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle to load parameters
    ros::NodeHandle ph("~");

    //Loading parameters
    std::string cmd_vel, speed_controller_topic;
    std::string status_topic, change_status_topic;
    double max_velocity = 0.00f;
    ph.getParam("cmd_vel", cmd_vel);
    ph.getParam("speed_controller_topic", speed_controller_topic);
    ph.getParam("robot_status_topic", status_topic);
    ph.getParam("robot_change_status_topic", change_status_topic);
    ph.getParam("max_velocity", max_velocity);

    //Publisher for the node
    ros::Publisher speed = nh.advertise<geometry_msgs::Twist>(cmd_vel, 1);
    ros::Publisher status = nh.advertise<std_msgs::Int32>(change_status_topic, 1);

    //Instantiation of the callback object
    Controller controller(nh, speed, status, max_velocity);

    //Subscribers for the node
    ros::Subscriber sub_speed = nh.subscribe(speed_controller_topic, 1, &Controller::speedCallback, &controller);
    ros::Subscriber sub_status = nh.subscribe<std_msgs::Int32>(status_topic, 1, &Controller::statusCallback, &controller);

    //Waiting for all nodes to be ready
    ros::Duration(1).sleep();
    controller.speedControl();

    ros::spin();

    return 0;
}

#endif
