/**
  * This class creates a custom occupancy grid and can be used to simulate a sensor
  * @author Fabio Brea
  * @version 1.0
  */
#include <ros/ros.h>
#include <ros/console.h>
#include <cmath>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Empty.h>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <geometry_msgs/Pose.h>

#ifndef TEST_CPP
#define TEST_CPP

class Map_generator
{

private:

    ros::NodeHandle ph;
    ros::Publisher sensor_topic;

    int width;
    int height;
    int robot_x;
    int robot_y;
    double resolution;
    std::string frame_id;

public:

    /**
     * @brief Map_generator: Constructor for the object
     * @param ph: private nodehandle for the class
     * @param sensor_fake: publisher for the sensor
     */
    Map_generator(ros::NodeHandle &ph, ros::Publisher &sensor_fake)
    {
        this->ph = ph;
        this->sensor_topic = sensor_fake;
        ph.getParam("width", width);
        ph.getParam("height", height);
        ph.getParam("resolution", resolution);
        ph.getParam("fake_sensor_frame_id", frame_id);
        this->robot_x = std::floor(width / 2);
        this->robot_y = std::floor(height / 2);
    }

    /**
     * Callback that fuse the occupancy grid from different sensors and create the message for the robot
     */
    void send()
    {
        dyn_msgs::OccupancyGridWithPose msg;
        nav_msgs::OccupancyGrid message;
        message.header.frame_id = frame_id;
        message.info.height = height;
        message.info.width = width;
        message.info.resolution = resolution;

        std::vector<int8_t> map;

        for (int i = 0; i < width * height; ++i)
            map.push_back(0);

        for (int j = 0; j < ((height / 2) - 1); ++j)
            map.at((robot_y - j) * height + robot_x + 4) = 1;

        //map.at(robot_y * height + robot_x) = 0;
        message.data = map;

        msg.grid = message;
        geometry_msgs::Pose pose;
        pose.position.x = 0;
        pose.position.y = 0;
        pose.position.z = 0;
        pose.orientation.x = 0;
        pose.orientation.y = 0;
        pose.orientation.z = 0;
        pose.orientation.w = 1;
        msg.pose = pose;
        sensor_topic.publish(msg);
        ros::Rate loop(10);
        loop.sleep();
    }

    /**
      * @brief voidCallback is needed to check if the sensor is active
      * @param empty_msg: a useless empty message
      */
    void voidCallback(const std_msgs::Empty::ConstPtr& empty_msg)
    {
        return;
    }

};

int main(int argc, char** argv) {

    ros::init(argc, argv, "fake_sensor");

    //Main nodehandle
    ros::NodeHandle nh;
    ros::NodeHandle ph("~");

    std::string maps_topic, sensor_topic;
    ph.getParam("maps_topic", maps_topic);
    ph.getParam("sensor_topic", sensor_topic);

    //Publisher for the node
    ros::Publisher pub_fake_map = nh.advertise<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1);

    //Instantiation of the callback object
    Map_generator fake_sender(ph, pub_fake_map);

    ros::Subscriber fake_sensor = nh.subscribe<std_msgs::Empty>(sensor_topic, 1, &Map_generator::voidCallback , &fake_sender);

    //Start sending maps
    while (ros::ok())
    {
        fake_sender.send();
        ros::spinOnce();
    }

    ros::spin();

    return 0;
}

#endif
