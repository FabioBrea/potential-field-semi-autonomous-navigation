/**
  * This class creates a udp/tcp connection with a server
  * @author Fabio Brea
  * @version 1.0
  */

#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stropts.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

//Ascii code for keyboard arrows
const int UP = 65;
const int DOWN = 66;
const int RIGHT = 67;
const int LEFT = 68;

int getCharacter()
{
    //Get current settings
    static struct termios new_setting;
    tcgetattr( STDIN_FILENO, &new_setting);

    //define new settings
    new_setting.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOPRT | ECHOKE | ICRNL);

    //apply new settings
    tcsetattr( STDIN_FILENO, TCSANOW, &new_setting);

    return getchar();
}

/**
  * This method listen for key strokes.
  */
int _kbhit()
{
    static struct termios new_setting;

    //Get current settings
    tcgetattr(STDIN_FILENO, &new_setting);

    //define new settings
    new_setting.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOPRT | ECHOKE | ICRNL);

    //apply new settings
    tcsetattr(STDIN_FILENO, TCSANOW, &new_setting);
    setbuf(stdin, NULL);

    //get key strokes, if any
    int bytesWaiting;
    ioctl(STDIN_FILENO, FIONREAD, &bytesWaiting);

    return bytesWaiting;
}
static struct termios keyboard_settings;

int main(int argc, char *argv[])
{
    std::cout << " _____________________________________________________________________ " << std::endl;
    std::cout << "|                                                                     |" << std::endl;
    std::cout << "|                                                                     |" << std::endl;
    std::cout << "|         This program sends key strokes input to the server.         |" << std::endl;
    std::cout << "|               Press the arrow keys to move the robot.               |" << std::endl;
    std::cout << "|        Press p to pause the system, r to resume and q to quit       |" << std::endl;
    std::cout << "|                                                                     |" << std::endl;
    std::cout << "|_____________________________________________________________________|" << std::endl;
    std::cout << std::endl;

    int sock;
    int status;
    struct addrinfo hints, *server_info, *info;

    if (argc != 3)
    {
        fprintf(stderr,"Insert the ip and port of the server\n");
        return 1;
    }
    std::string server_ip = argv[1];
    std::string port = argv[2];

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    //Find the server
    if ((status = getaddrinfo(server_ip.c_str(), port.c_str(), &hints, &server_info)) != 0)
    {
        fprintf(stderr, "Error: %s\n", gai_strerror(status));
        return 1;
    }

    //Loop through all the results and make a socket
    for(info = server_info; info != NULL; info = info->ai_next)
    {
        if ((sock = socket(info->ai_family, info->ai_socktype, info->ai_protocol)) == -1)
            continue;
        break;
    }

    if (info == NULL)
    {
        fprintf(stderr, "Socket not created.\n");
        return 2;
    }
    //Save keyboard settings
    tcgetattr(STDIN_FILENO, &keyboard_settings);

    std::cout << "Press a key now to move the robot. Press 'p' to pause, 'r' to resume, q' to quit." << std::endl;

    while(true)
    {
        int character = getCharacter();
        std::stringstream ss;
        ss << character;
        std::string msg = ss.str();

        int numbytes;
        if (character == UP || character == DOWN || character == RIGHT || character == LEFT || character == 'q' || character == 'r' || character == 'p')
        {
            if ((numbytes = sendto(sock, msg.c_str(), msg.length(), 0, info->ai_addr, info->ai_addrlen)) == -1)
            {
                fprintf(stderr, "Packet lost.\n");
                return 1;
            }

            //Getting character name, only to inform the user
            std::string name;
            if (character == UP)
                name = "UP";
            else if (character == DOWN)
                name = "DOWN";
            else if (character == RIGHT)
                name = "RIGHT";
            else if (character == LEFT)
                name = "LEFT";
            else if (character == 'r')
                name = "READY";
            else if (character == 'p')
                name = "PAUSE";
            else
                name = "QUIT";

            printf("Sending %s command to %s\n", name.c_str(), server_ip.c_str());

            //Waiting time between input
            time_t start = time(0);
            while((time(0) - start) < 3)
            {
                if(_kbhit())
                    getCharacter(); //Delete input from cin
            }

            //Restore keyboard settings
            tcsetattr( STDIN_FILENO, TCSANOW, &keyboard_settings);

            if (character == 'q')
            {
                printf("Exiting.\n");
                break;
            }
            std::cout << "Done." << std::endl;
            std::cout << std::endl;
            std::cout << "Press a key now to move the robot. Press 'p' to pause, 'r' to resume, q' to quit." << std::endl;
        }
    }

    freeaddrinfo(server_info);
    close(sock);

    return 0;
}
