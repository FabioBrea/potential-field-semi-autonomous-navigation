/**
  * Class to launch a Bumpers node
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_event/Bumpers.hpp>

int main(int argc, char** argv) {

    ros::init(argc, argv, "bumpers_sensor");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    std::string maps_topic, change_status_topic;
    ph.getParam("maps_topic" , maps_topic);
    ph.getParam("robot_change_status_topic", change_status_topic);

    //Topic on which bumpers has to publish
    ros::Publisher pub_obstacle = nh.advertise<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1);
    ros::Publisher status_pub = nh.advertise<std_msgs::Int32>(change_status_topic, 1);
    ros::ServiceClient pose_client = nh.serviceClient<dyn_utilities::getPose>("get_pose");

    event::Bumpers bumpers(nh, ph, pub_obstacle, status_pub, pose_client);

    ros::Subscriber sub_bumpers = nh.subscribe<sensor_msgs::PointCloud2>("/mobile_base/sensors/bumper_pointcloud", 1, &event::Bumpers::callback, &bumpers);

    ros::spin();

    return 0;
}
