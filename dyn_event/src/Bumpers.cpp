/**
  * Implementation of a Bumper class that creates a dynamic occupancy grid after a bump
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_event/Bumpers.hpp>

#ifndef BUMPERS_CPP
#define BUMPERS_CPP

namespace event
{
/**
     * @brief Bumpers: Constructor for the bumpers
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle, for getting parameters from yaml
     * @param pub: the publisher of the node
     * @param pose_client: can call the service to get the current pose
     */
Bumpers::Bumpers(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, ros::Publisher status_pub, ros::ServiceClient pose_client)
{
    this->nh = nh;
    this->ph = ph;
    this->pub = pub;
    this->status_pub = status_pub;
    this->pose_client = pose_client;

    ph.getParam("width", width);
    ph.getParam("height", height);
    ph.getParam("resolution", resolution);
    ph.getParam("bumpers_frame_id", bumpers_frame_id);
    ph.getParam("bumpers_publishing_time", publishing_time);
    ph.getParam("x_distance_offset", x_offset);
    ph.getParam("y_distance_offset", y_offset);
    ph.getParam("sensor_topic", sensor_topic);

    robot_x = std::floor(width / 2.00f);
    robot_y = std::floor(height / 2.00f);
}

/**
     * @brief Callback: gets a pointcloud2 message and create and publish an Occupancy Grid
     * @param point_cloud2_msg: the message from the bumpers
     */
void Bumpers::callback(const sensor_msgs::PointCloud2::ConstPtr& point_cloud2_msg)
{

    //Scan through pointcloud to get the new occupancy grid
    sensor_msgs::PointCloud2ConstPtr cloud = point_cloud2_msg;

    std::vector<int8_t> map;
    map.reserve(width * height);
    int obstacle_detected = 0;

    //Initialize a map without obstacles
    for (int i = 0; i < width * height; ++i)
        map.push_back(0);

    // Iterate through pointcloud
    for (sensor_msgs::PointCloud2ConstIterator<float>
         iter_x(*cloud, "x"), iter_y(*cloud, "y"), iter_z(*cloud, "z");
         iter_x != iter_x.end();
         ++iter_x, ++iter_y, ++iter_z)
    {

        if (std::isnan(*iter_x) || std::isnan(*iter_y) || std::isnan(*iter_z))
        {
            ROS_DEBUG("Rejected for nan in point(%f, %f, %f)\n", *iter_x, *iter_y, *iter_z);
            continue;
        }

        double range = hypot(*iter_x, *iter_y);
        double angle = atan2(*iter_y, *iter_x);

        //Exclude out of range points
        if(range > (std::max(width, height) - robot_x) * resolution)
            continue;

        //Creating an obstacle in the Occupancy Grid
        int signObsX = 0;
        int signObsY = 0;

        if (std::sin(angle) > 0)
            signObsX = 1;
        else if (std::sin(angle) < 0)
            signObsX = -1;

        if (std::cos(angle) > 0)
            signObsY = 1;
        else if (std::cos(angle) < 0)
            signObsY = -1;

        int obstacle_position_x = robot_x + signObsX * std::ceil(std::abs((range / resolution) * std::sin(angle)));
        int obstacle_position_y = robot_y + signObsY * std::ceil(std::abs((range / resolution) * std::cos(angle)));

        if(angle == 0)
        {
            obstacle_position_x -= 3;
            obstacle_position_y += 5;
        }
        //Revert to get the Occupancy Grid coherent to robot heading
        obstacle_position_x = (width - obstacle_position_x) - 1;
        obstacle_position_y = (height - obstacle_position_y) - 1;
        obstacle_detected++;

        for(int i = -1 ; i < 2; ++i)
        {
            for(int j = -1; j < 2; ++j)
            {
                map.at((((obstacle_position_y - signObsY * y_offset) + i) * width) + (obstacle_position_x - signObsX * x_offset) + j) = 1;
            }
        }
    }

    //Creation and publishing of the Obstacle message
    nav_msgs::OccupancyGrid grid;
    nav_msgs::MapMetaData info;

    //Initialize information about the map
    info.map_load_time = ros::Time::now();
    info.resolution = resolution;
    info.width = width;
    info.height = height;

    //Initialize occupancy grid parameters
    grid.header.frame_id = bumpers_frame_id;
    grid.header.stamp = ros::Time::now();
    grid.info = info;
    grid.data = map;

    //Publish the message for an opportune time, if we have obstacles -not true when bumper released-
    if(obstacle_detected > 0)
    {
        //Collision detected, stop the robot
        std_msgs::Int32 collide_msgs;
        collide_msgs.data = COLLIDED;

        while(!status_pub.getNumSubscribers() > 0)
            ros::Duration(0.01).sleep();

        status_pub.publish(collide_msgs);
        ros::spinOnce();

        //Subscribe to active topics
        ros::Subscriber sub_sensor = nh.subscribe<std_msgs::Empty>(sensor_topic, 1, &Bumpers::voidCallback, this);

        dyn_utilities::getPose srv;
        pose_client.call(srv);

        dyn_msgs::OccupancyGridWithPose msg;
        msg.grid = grid;
        msg.pose = srv.response.current_pose;

        //Publish the collided obstacle
        ros::Time start = ros::Time::now();
        while((ros::Time::now() - start).toSec() < publishing_time)
        {
            while(!pub.getNumSubscribers() > 0)
                ros::Duration(0.01).sleep();

            pub.publish(msg);
            ros::spinOnce();
        }

        //Unsubscribe from active sensors topic
        sub_sensor.shutdown();

    }

}

/**
  * @brief voidCallback is needed to check if the sensor is active
  * @param empty_msg: a useless empty message
  */
void Bumpers::voidCallback(const std_msgs::Empty::ConstPtr& empty_msg)
{
    return;
}

}
#endif
