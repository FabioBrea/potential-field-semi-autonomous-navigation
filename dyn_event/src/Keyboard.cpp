/**
  * Implementation of a Keyboard class that it's used to get a semi-autonomous navigation
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_event/Keyboard.hpp>

#ifndef KEYBOARD_CPP
#define KEYBOARD_CPP

namespace event
{

/**
 * @brief Keyboard: Void constructor for the keyboard
 */
Keyboard::Keyboard(){}

/**
  * @brief Keyboard: Constructor for the keyboard
  * @param nh: the main nodehandle
  * @param ph: the private nodehandle, for getting parameters from yaml
  * @param pub: the publisher of the node
  * @param pose_client: can call the service to get the current pose
  */
Keyboard::Keyboard(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, ros::Publisher status_pub, ros::ServiceClient pose_client)
{
    this->nh = nh;
    this->ph = ph;
    this->pub = pub;
    this->status_pub = status_pub;
    this->pose_client = pose_client;

    ph.getParam("width", width);
    ph.getParam("height", height);
    ph.getParam("resolution", resolution);
    ph.getParam("keyboard_frame_id", keyboard_frame_id);
    ph.getParam("keyboard_publishing_time", publishing_time);
    ph.getParam("connection_type", connection_type);
    ph.getParam("attractor_distance", attractor_distance);

    std::string port;
    ph.getParam("port", port);

    robot_x = std::floor(width / 2.00f);
    robot_y = std::floor(height / 2.00f);

    //Create a socket if requested
    if(connection_type == "REMOTE")
    {
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_flags = AI_PASSIVE;

        if ((status = getaddrinfo(NULL, port.c_str(), &hints, &server_info)) != 0)
        {
            ROS_ERROR("[HOST ADDRESS]: %s", gai_strerror(status));
            ros::shutdown();
        }

        // Bind the first through the results
        for(info = server_info; info != NULL; info = info->ai_next)
        {
            if ((sock = socket(info->ai_family, info->ai_socktype, info->ai_protocol)) == -1)
            {
                ROS_ERROR("[SOCKET]: Can't create a socket");
                continue;
            }

            if (bind(sock, info->ai_addr, info->ai_addrlen) == -1)
            {
                close(sock);
                ROS_ERROR("[SOCKET]: Error in binding");
                continue;
            }

            break;
        }

        //Cannot bind
        if (info == NULL)
        {
            ROS_ERROR("[SOCKET]: Socket not binded");
            ros::shutdown();
        }

        ROS_INFO("[KEYBOARD] Created a socket, waiting for command from the client.");
    }
    else if(connection_type == "LOCAL")
    {
        ROS_INFO("[KEYBOARD] Waiting for a command.");
    }
    else
    {
        ROS_ERROR("[KEYBOARD] Bad connection type. Can be only LOCAL or REMOTE");
        ros::shutdown();
    }

}

/**
  * @brief getCharacter: get a character from keyboard without blocking other processes
  * @return the ascii code of the read character
  */
int Keyboard::getCharacter()
{
    //save old settings
    static struct termios new_setting;
    tcgetattr( STDIN_FILENO, &new_setting);

    //define new settings
    new_setting.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ECHOPRT | ECHOKE | ICRNL);

    //apply new settings
    tcsetattr( STDIN_FILENO, TCSANOW, &new_setting);

    return getchar();
}

/**
  * @brief sendOccupancyGrid:
  * @param heading: (turnback, goforward, stay) the heading direction, depends from the arrow key pressed
  * @param turn: (left, right, stay) the turning direction, depends from the arrow key pressed
  */
void Keyboard::sendOccupancyGrid(const int heading, const int turn)
{

    ros::Duration(1).sleep();

    nav_msgs::OccupancyGrid grid;
    nav_msgs::MapMetaData info;

    //Initialize information about the map
    info.map_load_time = ros::Time::now();
    info.resolution = resolution;
    info.width = width;
    info.height = height;

    //Initialize occupancy grid parameters
    grid.header.frame_id = keyboard_frame_id;
    grid.header.stamp = ros::Time::now();
    grid.info = info;

    std::vector<int8_t> map;

    //Initialize a map without obstacles
    for (int i = 0; i < width * height; ++i)
        map.push_back(0);

    //Create an attractor in the direction defined from input
    if (heading == TURNBACK || heading == GOFORWARD)
        map.at((robot_y + heading * attractor_distance) * width + robot_x) = -1;
    else
        map.at(robot_y * width + (robot_x + turn * attractor_distance)) = -1;

    grid.data = map;

    dyn_utilities::getPose srv;
    pose_client.call(srv);
    
    dyn_msgs::OccupancyGridWithPose msg;
    msg.grid = grid;
    msg.pose = srv.response.current_pose;

    //Publish the message for an opportune time
    ros::Time start = ros::Time::now();
    while((ros::Time::now() - start).toSec() < publishing_time)
        pub.publish(msg);

    ros::Duration(0.1).sleep();

    //Return to autonomous drive
    std_msgs::Int32 run_msg;
    run_msg.data = RUNNING;
    status_pub.publish(run_msg);

}

/**
  * @brief listener: gets the character from keyboard and send a message with an Occupancy Grid
  */
void Keyboard::listener()
{
    const int bufSize = 100;
    char msg_buffer[bufSize];

    tcgetattr(STDIN_FILENO, &keyboard_setting);

    //Get characters from input
    while (ros::ok())
    {
        int character;

        if (connection_type == "LOCAL")
        {
            character = getCharacter();
        }
        else
        {
            addr_len = sizeof client_addr;
            if ((numbytes = recvfrom(sock, msg_buffer, bufSize - 1 , 0, (struct sockaddr *)&client_addr, &addr_len)) == -1)
            {
                ROS_ERROR("[CONNECTION] Error in receiving the packets");
                ros::shutdown();
            }

            msg_buffer[numbytes] = '\0'; //Put a null character to end the string
            character = atoi(msg_buffer);
        }

        //Create an Occupancy Grid and publish, using the right robot states
        if (character == UP || character == DOWN || character == LEFT || character == RIGHT || character == 'q' || character == 'p' || character == 'r')
        {
            //Control the robot manually
            std_msgs::Int32 manual_msg;
            manual_msg.data = MANUAL;
            status_pub.publish(manual_msg);
            ros::Duration(0.1).sleep();

            if (character == UP)
            {
                ROS_INFO("UP");
                sendOccupancyGrid(GOFORWARD, STAY);
            }
            else if (character == DOWN)
            {
                ROS_INFO("DOWN");
                sendOccupancyGrid(TURNBACK, STAY);
            }
            else if (character == RIGHT)
            {
                ROS_INFO("RIGHT");
                sendOccupancyGrid(STAY, TURNRIGHT);
            }
            else if (character == LEFT)
            {
                ROS_INFO("LEFT");
                sendOccupancyGrid(STAY, TURNLEFT);
            }
            else if (character == 'q')
            {
                ROS_INFO("Stopping the system. Stopping command acquired");
                std_msgs::Int32 stopping_msgs;
                stopping_msgs.data = STOPPED;
                status_pub.publish(stopping_msgs);
                ros::Duration(0.1).sleep();
                tcsetattr( STDIN_FILENO, TCSANOW, &keyboard_setting);
                ros::shutdown();
                break;
            }
            else if (character == 'p')
            {
                ROS_INFO("System in pause");
                std_msgs::Int32 pausing_msgs;
                pausing_msgs.data = PAUSED;
                status_pub.publish(pausing_msgs);
                ros::Duration(0.1).sleep();
            }
            else if (character == 'r')
            {
                ROS_INFO("Resume system from pause");
                std_msgs::Int32 running_msgs;
                running_msgs.data = READY;
                status_pub.publish(running_msgs);
                ros::Duration(0.1).sleep();
            }

            tcsetattr( STDIN_FILENO, TCSANOW, &keyboard_setting);
        }
    }
}

/**
 * @brief restoreKeyboard return the default settings for the keyboard
 */
void Keyboard::restoreKeyboard()
{
    tcsetattr( STDIN_FILENO, TCSANOW, &keyboard_setting);
}

}
#endif
