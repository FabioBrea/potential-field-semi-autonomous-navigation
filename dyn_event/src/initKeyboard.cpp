/**
  * Class to launch a Keyboard node
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_event/Keyboard.hpp>
#include <signal.h>

event::Keyboard *keyboard;

/**
 * @brief kill restore keyboard settings and the shutdown ros
 * @param sig: the signal to kill the process
 */
void kill(int sig)
{
    keyboard->restoreKeyboard();
    ros::shutdown();
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "keyboard", ros::init_options::NoSigintHandler);

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    std::string maps_topic, change_status_topic;
    ph.getParam("maps_topic" , maps_topic);
    ph.getParam("robot_change_status_topic", change_status_topic);

    //Topic on which keyboard has to publish
    ros::Publisher pub_obstacle = nh.advertise<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1);
    ros::Publisher status_pub = nh.advertise<std_msgs::Int32>(change_status_topic, 1);
    ros::ServiceClient pose_client = nh.serviceClient<dyn_utilities::getPose>("get_pose");

    keyboard = new event::Keyboard(nh, ph, pub_obstacle, status_pub, pose_client);

    signal(SIGINT, kill);

    //Start acquisition of characters
    keyboard->listener();

    return 0;
}
