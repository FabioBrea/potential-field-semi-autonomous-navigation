/**
  * Header for the keyboard class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <termios.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/MapMetaData.h>
#include <dyn_utilities/System.hpp>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <geometry_msgs/Pose.h>
#include <dyn_utilities/getPose.h>
#include <std_msgs/Int32.h>

#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

//Ascii code for keyboard arrows
const int UP = 65;
const int DOWN = 66;
const int RIGHT = 67;
const int LEFT = 68;

const int TURNLEFT = -1;
const int TURNRIGHT = 1;
const int STAY = 0;
const int TURNBACK = 1;
const int GOFORWARD = -1;

namespace event
{

/**
 * @brief The Keyboard class gets a character from keyboard and publish an OccupancyGrid with an attractor on the defined direction
 */
class Keyboard
{

public:

    /**
     * @brief Keyboard: Void constructor for the keyboard
     */
    Keyboard();

    /**
     * @brief Keyboard: Constructor for the keyboard
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle, for getting parameters from yaml
     * @param pub: the publisher of the node
     * @param pose_client: can call the service to get the current pose
     */
    Keyboard(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, ros::Publisher status_pub, ros::ServiceClient pose_client);

private:

    //Parameters from yaml file. Define the Occupancy Grid
    int width;
    int height;
    float resolution;
    int robot_x;
    int robot_y;
    std::string keyboard_frame_id;
    struct termios keyboard_setting;

    //How long to publish a message, in seconds. From yaml file
    double publishing_time;
    std::string connection_type;
    int attractor_distance;

    //Publishers for the node
    ros::Publisher pub;
    ros::Publisher status_pub;

    //Nodehandles for this class
    ros::NodeHandle nh;
    ros::NodeHandle ph;
    ros::ServiceClient pose_client;

    int sock;
    int status;
    int numbytes;
    socklen_t addr_len;
    struct addrinfo hints, *server_info, *info;
    struct sockaddr_storage client_addr;

public:

    /**
     * @brief listener: gets the character from keyboard and send a message with an Occupancy Grid
     */
    void listener();

    /**
     * @brief restoreKeyboard return the default settings for the keyboard
     */
    void restoreKeyboard();

private:

    /**
     * @brief getCharacter: get a character from keyboard without blocking other processes
     * @return the ascii code of the read character
     */
    int getCharacter();

    /**
      * @brief sendOccupancyGrid:
      * @param heading: (turnback, goforward, stay) the heading direction, depends from the arrow key pressed
      * @param turn: (left, right, stay) the turning direction, depends from the arrow key pressed
      */
    void sendOccupancyGrid(const int heading, const int turn);

};

}
#endif
