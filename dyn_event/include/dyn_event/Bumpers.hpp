/**
  * Header for the bumpers class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <cmath>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/MapMetaData.h>
#include <dyn_utilities/System.hpp>
#include <std_msgs/Int32.h>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <dyn_utilities/getPose.h>
#include <geometry_msgs/Pose.h>

#ifndef BUMPERS_HPP
#define BUMPERS_HPP

namespace event
{

/**
 * @brief The Bumpers class gets a pointcloud2 message and publish an OccupancyGrid with obstacles at the selected bumper
 */
class Bumpers
{

public:

    /**
     * @brief Bumpers: Constructor for the bumpers
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle, for getting parameters from yaml
     * @param pub: the publisher of the node
     * @param pose_client: can call the service to get the current pose
     */
    Bumpers(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, ros::Publisher status_pub, ros::ServiceClient pose_client);

private:

    //Parameters from yaml file. Define the Occupancy Grid.
    int width;
    int height;
    float resolution;
    int robot_x;
    int robot_y;
    std::string bumpers_frame_id;
    std::string sensor_topic;

    //Publishers for the node
    ros::Publisher pub;
    ros::Publisher status_pub;
    ros::ServiceClient pose_client;
    double publishing_time;
    int x_offset;
    int y_offset;

    //Nodehandles for this class
    ros::NodeHandle nh;
    ros::NodeHandle ph;

public:

    /**
     * @brief Callback: gets a pointcloud2 message and create and publish an Occupancy Grid
     * @param point_cloud2_msg: the message from the bumpers
     */
    void callback(const sensor_msgs::PointCloud2::ConstPtr& point_cloud2_msg);

    /**
      * @brief voidCallback is needed to check if the sensor is active
      * @param empty_msg: a useless empty message
      */
    void voidCallback(const std_msgs::Empty::ConstPtr& empty_msg);

};

}
#endif
