/**
  * This class uses Gstreamer to stream the video from an image topic, from Kinect v2, to a client
  * @author Fabio Brea
  * @version 1.0
  */


#include <ros/ros.h>
#include <ros/console.h>
#include <signal.h>
#include <sensor_msgs/Image.h>
#include <arpa/inet.h>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>

class ImageAcquisitor
{

public:

    static const int width = 960;
    static const int height = 540;
    static const int num_channels = 3;

    //Gstreamer elements
    GstElement *pipeline, *appsrc, *conv, *videoenc, *payloader, *udpsink;

    ros::NodeHandle ph;
    std::string client_ip;
    int port;

    //The image to put in the buffer for streaming
    u_int8_t *image;

    /**
     * @brief ImageAcquisitor void constructor for the object
     */
    ImageAcquisitor(){}

    /**
     * @brief ImageAcquisitor constructor for the object
     * @param nh: the main nodehandle
     * @param ph: a private nodehandle, to take parameters
     */
    ImageAcquisitor(ros::NodeHandle ph)
    {
        this->ph = ph;
        image = new u_int8_t[width * height * num_channels];
        ph.param<std::string>("client_ip", client_ip, "127.0.0.1");
        ph.param<int>("video_port", port, 1024);

        //Check for correct ip
        struct sockaddr_in sa;
        if(inet_pton(AF_INET, client_ip.c_str(), &(sa.sin_addr)) != 1)
        {
            ROS_ERROR("The ip you inserted is not correct. Please, insert a valid ipv4.");
        }

        //Check for correct port
        if (port < 1024 || port > 65535)
        {
            ROS_ERROR("VIDEO Port number not available. Please, insert a value between 1024 and 65535");
        }
    }

    /**
     * @brief image_callback takes images from a sensor and save them in a variable
     * @param image_msg: the message from the sensor
     */
    void image_callback(const sensor_msgs::Image::ConstPtr& image_msg)
    {
        for (int i = 0; i < width * height * num_channels; ++i)
        {
            image[i] = image_msg->data[i];
        }
    }

    /**
     * @brief feed_buffer put images in the buffer that is currently streaming
     * @param appsrc: the source that create the stream
     * @param unused_size: unused variable
     * @param user_data: pointer to the images
     */
    static void feed_buffer(GstElement *appsrc, guint unused_size, gpointer user_data)
    {
        GstBuffer *buffer;
        guint size;
        GstFlowReturn ret;
        static GstClockTime timestamp = 0;

        //Update image
        ros::spinOnce();

        //The size of the image we put in the buffer
        size = width * height * num_channels;

        buffer = gst_buffer_new_wrapped_full(GST_MEMORY_FLAG_READONLY, user_data, size, 0, size, NULL, NULL);

        GST_BUFFER_PTS(buffer) = timestamp;
        GST_BUFFER_DURATION(buffer) = gst_util_uint64_scale_int (1, GST_SECOND, 15);

        timestamp += GST_BUFFER_DURATION (buffer);

        //Push buffer in the stream
        g_signal_emit_by_name(appsrc, "push-buffer", buffer, &ret);
        gst_buffer_unref(buffer);
    }

    /**
     * @brief start: create and start the pipeline
     */
    void start()
    {
        GstBus *bus;
        GstMessage *gstmsg;

        //Create pipeline
        pipeline = gst_pipeline_new ("kinect2-sender-pipeline");

        //Take the video from external source
        appsrc = gst_element_factory_make ("appsrc", "source");

        //setup caps
        g_object_set(G_OBJECT (appsrc), "caps",
                        gst_caps_new_simple("video/x-raw",
                                            "format", G_TYPE_STRING, "BGR",
                                            "width", G_TYPE_INT, width,
                                            "height", G_TYPE_INT, height,
                                            "framerate", GST_TYPE_FRACTION, 15, 1,
                                            NULL),
                     NULL);

        //Convert the video, necessary to encode
        conv = gst_element_factory_make ("videoconvert", "conv");

        //Encode the video
        videoenc = gst_element_factory_make ("x264enc", "videoenc");
        g_object_set(G_OBJECT(videoenc),
                        "bitrate", 1000,
                        "threads", 2,
                        "speed-preset", 1,
                        "byte-stream", true,
                     NULL);

        //Payload-encode the video into rtp packets
        payloader = gst_element_factory_make ("rtph264pay", "payloader");

        //Create an udp socket
        udpsink = gst_element_factory_make("udpsink", "udpsink");
        g_object_set(G_OBJECT(udpsink),
                        "host", client_ip.c_str(),
                        "port", port,
                     NULL);

        gst_bin_add_many (GST_BIN (pipeline), appsrc, conv, videoenc, payloader, udpsink, NULL);
        gst_element_link_many (appsrc, conv, videoenc, payloader, udpsink, NULL);

        //Setup appsrc
        g_object_set (G_OBJECT (appsrc),
                        "stream-type", 0,
                        "format", GST_FORMAT_TIME,
                      NULL);
        g_signal_connect (appsrc, "need-data", G_CALLBACK (feed_buffer), (gpointer)(image));

        //Play the pipeline
        gst_element_set_state (pipeline, GST_STATE_PLAYING);
        ROS_INFO("Playing...");

        //Waiting for errors or end of stream
        bus = gst_element_get_bus (pipeline);
        gstmsg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, (GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

        //Parse the received message
        if (gstmsg != NULL)
        {
            GError *err;
            gchar *debug_info;

            if(GST_MESSAGE_TYPE (gstmsg) == GST_MESSAGE_ERROR)
            {
                gst_message_parse_error(gstmsg, &err, &debug_info);
                ROS_ERROR("Error received from element %s: %s", GST_OBJECT_NAME (gstmsg->src), err->message);
                ROS_ERROR("Debugging information: %s", debug_info ? debug_info : "none");
                g_clear_error(&err);
                g_free(debug_info);
            }
            else if(GST_MESSAGE_TYPE (gstmsg) ==  GST_MESSAGE_EOS)
            {
                ROS_INFO("End-Of-Stream reached.");
            }
            else// We only asked for ERRORs and EOS, shouldn't arrive here
            {
                ROS_ERROR("Unexpected message received.\n");
            }

            gst_message_unref (gstmsg);
        }

        //Deallocate variables
        gst_object_unref (bus);
        gst_element_set_state (pipeline, GST_STATE_NULL);
        gst_object_unref (GST_OBJECT (pipeline));
    }

    /**
     * @brief endStream sends an EOS message to Gstreamer
     */
    void endStream()
    {
        GstEvent*  event = gst_event_new_eos();
        gst_element_send_event(pipeline, event);
    }

};

ImageAcquisitor* image_acquisitor;

/**
 * @brief killProcess kill Gstreamer first and then ros
 * @param sig: the signal to kill the process
 */
void killProcess(int sig)
{
    image_acquisitor->endStream();
    ros::shutdown();
}


int main (int argc, char *argv[])
{
    if(ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug))
    {
        ros::console::notifyLoggerLevelsChanged();
    }

    //Initialize ros and gstreamer
    ros::init(argc, argv, "Kinect2_Server", ros::init_options::NoSigintHandler);
    gst_init (&argc, &argv);

    //Ros variables
    ros::AsyncSpinner spinner(0);
    ros::NodeHandle nh;
    ros::NodeHandle ph("~");
    std::string kinect2_topic;
    ph.getParam("kinect2_topic", kinect2_topic);

    //Allocate object and subscribe to kinect topic
    image_acquisitor = new ImageAcquisitor(ph);
    ros::Subscriber sub_image = nh.subscribe<sensor_msgs::Image>(kinect2_topic, 1, &ImageAcquisitor::image_callback, image_acquisitor);

    //Overwrite sigint behaviour, otherwise Gstreamer won't stop
    signal(SIGINT, killProcess);
    image_acquisitor->start();

    spinner.start();
    return 0;
}
