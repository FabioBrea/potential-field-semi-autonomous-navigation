/**
  * This class uses Gstreamer to receive a video and visualize it
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <gst/gst.h>

int main(int argc, char *argv[])
{
    if(ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug))
    {
        ros::console::notifyLoggerLevelsChanged();
    }

    GstElement *pipeline, *udpsrc, *filter, *depayload, *queue, *decoder, *converter, *scaler, *avimux, *videosink;
    GstBus *bus;
    GstCaps *cap;
    GstMessage *msg;
    GstStateChangeReturn ret;

    //Initialize ROS and GStreamer
    ros::init(argc, argv, "video_client");
    ros::NodeHandle ph("~");
    gst_init (&argc, &argv);

    bool save_video = false;
    std::string save_location;
    ph.getParam("save_video", save_video);
    ph.getParam("save_location", save_location);

    //Create the elements of the pipeline

    //Create the udp socket
    int port;
    ph.param<int>("video_port", port, 1024);
    if(port < 1024 || port > 65535)
    {
        ROS_ERROR("Port number not available. Please, insert a value between 1024 and 65535");
        return -1;
    }

    udpsrc = gst_element_factory_make ("udpsrc", "udpsrc");
    g_object_set (G_OBJECT(udpsrc),
                  "port", port,
                  NULL);

    //Filters the video
    cap = gst_caps_new_simple("application/x-rtp",
                              "media", G_TYPE_STRING, "video",
                              "clock-rate", G_TYPE_INT, 90000,
                              "encoding-name", G_TYPE_STRING, "H264",
                              "payload", G_TYPE_INT, 255,
                              NULL);
    filter = gst_element_factory_make("capsfilter","filter");
    g_object_set(G_OBJECT(filter), "caps", cap, NULL);

    //Depayload the video
    depayload = gst_element_factory_make("rtph264depay", "depayload");

    //Put everything in a queue, help prevents laggy videos
    queue = gst_element_factory_make("queue", "queue");

    //Decode the video
    decoder = gst_element_factory_make ("avdec_h264", "decoder");

    //Convert and scale the video, to link to the visualizer
    converter = gst_element_factory_make ("videoconvert", "converter");
    scaler = gst_element_factory_make ("videoscale", "scaler");
    if(save_video)
    {
        //Save the video
        avimux = gst_element_factory_make ("avimux", "avimux");
        videosink = gst_element_factory_make ("filesink", "videosink");
        g_object_set(G_OBJECT(videosink),
                     "location", save_location.c_str(),
                     NULL);

        //Link the elements together

        //Create an empty pipeline
        pipeline = gst_pipeline_new ("video-receiver-pipeline");

        if (!pipeline || !udpsrc || !filter || !depayload || !queue || !decoder || !converter || !scaler || !videosink /*|| !avimux*/)
        {
            ROS_ERROR("Not all elements could be created.");
            return -1;
        }

        //Build the pipeline
        gst_bin_add_many (GST_BIN (pipeline), udpsrc, filter, depayload, queue, decoder, converter, scaler, avimux, videosink, NULL);

        if (!gst_element_link_many (udpsrc, filter, depayload, queue, decoder, converter, scaler, avimux, videosink, NULL))
        {
            ROS_ERROR("Failed to link elements!");
            gst_object_unref (pipeline);
            return -1;
        }
    }
    else
    {
        //Visualize the video
        videosink = gst_element_factory_make ("xvimagesink", "videosink");
        g_object_set(G_OBJECT(videosink),
                     "sync", false,
                     NULL);

        //Link the elements together

        //Create an empty pipeline
        pipeline = gst_pipeline_new ("video-receiver-pipeline");

        if (!pipeline || !udpsrc || !filter || !depayload || !queue || !decoder || !converter || !scaler || !videosink )
        {
            ROS_ERROR("Not all elements could be created.");
            return -1;
        }

        //Build the pipeline
        gst_bin_add_many (GST_BIN (pipeline), udpsrc, filter, depayload, queue, decoder, converter, scaler, videosink, NULL);

        if (!gst_element_link_many (udpsrc, filter, depayload, queue, decoder, converter, scaler, videosink, NULL))
        {
            ROS_ERROR("Failed to link elements!");
            gst_object_unref (pipeline);
            return -1;
        }
    }

    //Start the pipeline
    ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);

    if (ret == GST_STATE_CHANGE_FAILURE)
    {
        ROS_ERROR("Unable to set the pipeline to the playing state.");
        gst_object_unref (pipeline);
        return -1;
    }

    ROS_INFO("Ready to receive a video. Could take some seconds to start visualize it.");

    //Wait until error or EOS
    bus = gst_element_get_bus (pipeline);
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, (GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

    //Parse the received message
    if (msg != NULL)
    {
        GError *err;
        gchar *debug_info;

        if(GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR)
        {
            gst_message_parse_error(msg, &err, &debug_info);
            std::string error = err->message;

            if(error == "Output window was closed")
            {
                ROS_INFO("User has closed the window. Closing the application.");
                return 0;
            }
            else
            {
                ROS_ERROR("Error received from element %s: %s", GST_OBJECT_NAME (msg->src), error.c_str());
                ROS_ERROR("Debugging information: %s", debug_info ? debug_info : "none");
            }
            g_clear_error(&err);
            g_free(debug_info);
        }
        else if(GST_MESSAGE_TYPE (msg) ==  GST_MESSAGE_EOS)
        {
            ROS_INFO("End-Of-Stream reached.");
        }
        else// We only asked for ERRORs and EOS, shouldn't arrive here
        {
            ROS_ERROR("Unexpected message received.\n");
        }

        gst_message_unref (msg);
    }

    //Free resources
    gst_object_unref (bus);
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    return 0;
}
