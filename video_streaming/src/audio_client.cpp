/**
  * This class uses Gstreamer to receive an audio and play it
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <gst/gst.h>

int main(int argc, char *argv[])
{
    if(ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug))
    {
        ros::console::notifyLoggerLevelsChanged();
    }

    GstElement *pipeline, *udpsrc, *depayload, *decoder, *amplify, *videosink;
    GstBus *bus;
    GstMessage *msg;
    GstStateChangeReturn ret;

    //Initialize ROS and GStreamer
    ros::init(argc, argv, "Audio_Client");
    ros::NodeHandle ph("~");
    gst_init (&argc, &argv);

    //Create the elements of the pipeline

    //Create the udp socket
    int port;
    ph.param<int>("audio_port", port, 1024);
    if(port < 1024 || port > 65535)
    {
        ROS_ERROR("CLIENT Port number not available. Please, insert a value between 1024 and 65535");
        return -1;
    }

    udpsrc = gst_element_factory_make ("udpsrc", "udpsrc");
    g_object_set (G_OBJECT(udpsrc),
                  "port", port,
                  "caps", gst_caps_new_simple("application/x-rtp",
                                              "media", G_TYPE_STRING, "audio",
                                              "clock-rate", G_TYPE_INT, 48000,
                                              "encoding-name", G_TYPE_STRING, "X-GST-OPUS-DRAFT-SPITTKA-00",
                                              NULL),
                  NULL);

    //Depayload the audio
    depayload = gst_element_factory_make("rtpopusdepay", "depayload");

    //Decode the audio
    decoder = gst_element_factory_make ("opusdec", "decoder");

    amplify = gst_element_factory_make ("audioamplify", "amplify");
    g_object_set(G_OBJECT(amplify),
                 "amplification", 10.0,
                 NULL);

    //Play the audio
    videosink = gst_element_factory_make ("autoaudiosink", "videosink");
    g_object_set(G_OBJECT(videosink),
                 "sync", false,
                 NULL);

    //Now we can link the elements together

    //Create an empty pipeline
    pipeline = gst_pipeline_new ("audio-receiver-pipeline");

    if (!pipeline || !udpsrc || !depayload || !decoder || !amplify || !videosink)
    {
        ROS_ERROR("Not all elements could be created.");
        return -1;
    }

    //Build the pipeline
    gst_bin_add_many (GST_BIN (pipeline), udpsrc, depayload, decoder, amplify, videosink, NULL);

    if (!gst_element_link_many (udpsrc, depayload, decoder, amplify, videosink, NULL))
    {
        ROS_ERROR("Failed to link elements!");
        gst_object_unref (pipeline);
        return -1;
    }

    //Start the pipeline
    ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);

    ROS_INFO("Ready to receive audio.");

    if (ret == GST_STATE_CHANGE_FAILURE)
    {
        ROS_ERROR("Unable to set the pipeline to the playing state.");
        gst_object_unref (pipeline);
        return -1;
    }

    //Wait until error or EOS
    bus = gst_element_get_bus (pipeline);
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, (GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

    //Parse the received message
    if (msg != NULL)
    {
        GError *err;
        gchar *debug_info;

        if(GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR)
        {
            gst_message_parse_error(msg, &err, &debug_info);
            ROS_ERROR("Error received from element %s: %s", GST_OBJECT_NAME (msg->src), err->message);
            ROS_ERROR("Debugging information: %s", debug_info ? debug_info : "none");
            g_clear_error(&err);
            g_free(debug_info);
        }
        else if(GST_MESSAGE_TYPE (msg) ==  GST_MESSAGE_EOS)
        {
            ROS_INFO("End-Of-Stream reached.");
        }
        else// We only asked for ERRORs and EOS, shouldn't arrive here
        {
            ROS_ERROR("Unexpected message received.\n");
        }

        gst_message_unref (msg);
    }

    //Free resources
    gst_object_unref (bus);
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    return 0;
}
