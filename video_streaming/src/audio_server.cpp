/**
  * This class uses Gstreamer to stream the audio from the laptop to a client
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <iostream>
#include <arpa/inet.h>
#include <gst/gst.h>

int main(int argc, char *argv[])
{
    if(ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug))
    {
        ros::console::notifyLoggerLevelsChanged();
    }

    GstElement *pipeline, *source, *convert, *resample, *audioenc, *payloader, *udpsink;
    GstCaps *cap;
    GstBus *bus;
    GstMessage *msg;
    GstStateChangeReturn ret;

    //Initialize Ros and GStreamer
    ros::init(argc, argv, "Audio_Server");
    ros::NodeHandle ph("~");
    gst_init (&argc, &argv);

    //Create the elements of the pipeline

    //Take video from webcam
    source = gst_element_factory_make ("pulsesrc", "source");

    //Convert the video, necessary to encode
    convert = gst_element_factory_make("audioconvert", "convert");

    //Convert the video, necessary to encode
    resample = gst_element_factory_make("audioresample", "resample");

    //Encode the video
    audioenc = gst_element_factory_make ("opusenc", "audioenc");

    //Payload-encode the video into rtp packets
    payloader = gst_element_factory_make ("rtpopuspay", "payloader");

    //Create the udp socket
    std::string client_ip;
    int port;
    ph.param<std::string>("client_ip", client_ip, "127.0.0.1");
    ph.param<int>("audio_port", port, 1024);

    //Check for correct ip
    struct sockaddr_in sa;
    if(inet_pton(AF_INET, client_ip.c_str(), &(sa.sin_addr)) != 1)
    {
        ROS_ERROR("The ip you inserted is not correct. Please, insert a valid ipv4.");
        return -1;
    }

    //Check for correct port
    if (port < 1024 || port > 65535)
    {
        ROS_ERROR(" SERVER Port number not available. Please, insert a value between 1024 and 65535");
        return -1;
    }

    udpsink = gst_element_factory_make("udpsink", "udpsink");
    g_object_set(G_OBJECT(udpsink),
                 "host", client_ip.c_str(),
                 "port", port,
                 NULL);

    //Now we can link the elements together

    //Create an empty pipeline
    pipeline = gst_pipeline_new ("audio-sender-pipeline");

    if (!pipeline || !source || !convert || !resample || !audioenc || !payloader || !udpsink)
    {
        ROS_ERROR("Not all elements could be created.");
        return -1;
    }

    //Build the pipeline
    gst_bin_add_many (GST_BIN (pipeline), source, convert, resample, audioenc, payloader, udpsink, NULL);

    if (!gst_element_link_many (source, convert, resample, audioenc, payloader, udpsink, NULL))
    {
        ROS_ERROR("Failed to link elements!");
    }

    //Stream the video
    ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);

    ROS_INFO("Sending audio to client...");

    if (ret == GST_STATE_CHANGE_FAILURE)
    {
        ROS_ERROR("Unable to set the pipeline to the playing state.");
        gst_object_unref (pipeline);
        return -1;
    }

    //ROS_INFO("Streaming...");

    //Wait until error or EOS
    bus = gst_element_get_bus (pipeline);
    msg = gst_bus_timed_pop_filtered (bus, GST_CLOCK_TIME_NONE, (GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

    //Parse the received message
    if (msg != NULL)
    {
        GError *err;
        gchar *debug_info;

        if(GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR)
        {
            gst_message_parse_error(msg, &err, &debug_info);
            ROS_ERROR("Error received from element %s: %s", GST_OBJECT_NAME (msg->src), err->message);
            ROS_ERROR("Debugging information: %s", debug_info ? debug_info : "none");
            g_clear_error(&err);
            g_free(debug_info);
        }
        else if(GST_MESSAGE_TYPE (msg) ==  GST_MESSAGE_EOS)
        {
            ROS_INFO("End-Of-Stream reached.");
        }
        else// We only asked for ERRORs and EOS, shouldn't arrive here
        {
            ROS_ERROR("Unexpected message received.\n");
        }

        gst_message_unref (msg);
    }

    //Deallocate variables
    gst_object_unref (bus);
    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    return 0;
}
