/**
  * Header for the laser class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <cmath>
#include <sensor_msgs/LaserScan.h>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/MapMetaData.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Empty.h>
#include <dyn_utilities/System.hpp>
#include <dyn_utilities/getPose.h>

#ifndef LASER_HPP
#define LASER_HPP

namespace sensor
{

/**
 * @brief The Laser class gets a laserscan message and publish an OccupancyGrid with obstacles at the right distance and angle
 */
class Laser
{

public:

    /**
     * @brief Laser: Constructor for a laser sensor
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle, for getting parameters from yaml
     * @param pub: the publisher of the node
     * @param frame: the frame id of the message
     * @param pose_client: can call the service to get the current pose
     */
    Laser(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, std::string frame, ros::ServiceClient pose_client);

private:

    //Parameters from a laserscan message
    float angle_min;
    float angle_max;
    float angle_inc;
    float range_min;
    float range_max;
    std::vector<float> ranges;

    //Parameters from yaml file. Define the Occupancy Grid.
    int width;
    int height;
    float resolution;
    int robot_x;
    int robot_y;
    std::string laser_frame_id;

    //Publisher for the node
    ros::Publisher pub;

    //Nodehandles for this class
    ros::NodeHandle nh;
    ros::NodeHandle ph;

    ros::ServiceClient pose_client;

    System robot;

public:

    /**
     * @brief laserCallback: gets a laser scan message and create and publish an Occupancy Grid
     * @param laser_msg: the message from laser scan
     */
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg);

    /**
     * @brief statusCallback check for change in robot state
     * @param status_msg: the state of the robot
     */
    void statusCallback(const std_msgs::Int32::ConstPtr& status_msg);

    /**
     * @brief voidCallback is needed to check if the sensor is active
     * @param empty_msg: a useless empty message
     */
    void voidCallback(const std_msgs::Empty::ConstPtr& empty_msg);


private:

    /**
     * @brief getObstacleMessage: create an OccupancyGrid message, given the distances and angles from laser messages
     * @param ranges: a vector of ranges of distances
     * @return the Occupancy Grid created
     */
    nav_msgs::OccupancyGrid getObstacleMessage(const std::vector<float>& ranges);

};

}
#endif
