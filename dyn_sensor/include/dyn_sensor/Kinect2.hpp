/**
  * Header for the kinect2 class
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/console.h>
#include <cmath>
#include <sensor_msgs/PointCloud2.h>
#include <dyn_msgs/OccupancyGridWithPose.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/MapMetaData.h>
#include <std_msgs/Empty.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/radius_outlier_removal.h>

#include <tf2_ros/transform_listener.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>

#ifndef KINECT2_HPP
#define KINECT2_HPP

namespace sensor
{

/**
 * @brief The Kinect2 class gets a pointcloud message from kinect2 and filters noise, then publishes the pointcloud filtered
 */
class Kinect2
{

public:

    /**
     * @brief Kinect2: Constructor for the kinect2 sensor, initialize filters with parameters from yaml file
     * @param nh: the main nodehandle
     * @param ph: the private nodehandle, for getting parameters from yaml
     * @param pub: the publisher of the node
     * @param transform: the transformation between two frames, to get a right transformation from pointcloudtolaserscan node
     */
    Kinect2(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, geometry_msgs::TransformStamped transform);

private:

    //Parameters that define the Occupancy Grid.
    std::string kinect2_frame_id;

    //Publisher for the node
    ros::Publisher pub;

    //Nodehandles for this class
    ros::NodeHandle nh;
    ros::NodeHandle ph;

    //Transformation of the frame of kinect2 to a consistent frame for transformation in laser scan
    geometry_msgs::TransformStamped transform;
    std::string destination_frame;

    //Filters for this class
    pcl::ConditionalRemoval<pcl::PointXYZ>* cond_rem;
    pcl::VoxelGrid<pcl::PointXYZ> vox;
    pcl::PassThrough<pcl::PointXYZ> pass_thr;
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    pcl::RadiusOutlierRemoval<pcl::PointXYZ> ror;

    //Parameters for radius outlier removal, here because they're changed during the filtering
    double near_std_dev;
    double far_std_dev;

public:

    /**
     * @brief filterCallback: gets a pointcloud message and filter it, then publish the resulting pointcloud message
     * @param point_cloud_msg: the message from kinect2
     */
    void filterCallback(const sensor_msgs::PointCloud2::ConstPtr& point_cloud);

    /**
      * @brief voidCallback is needed to check if the sensor is active
      * @param empty_msg: a useless empty message
      */
    void voidCallback(const std_msgs::Empty::ConstPtr& empty_msg);

};

}
#endif
