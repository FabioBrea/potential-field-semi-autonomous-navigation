/**
  * Class that transform a laserscan from kinect2 in an OccupancyGrid
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_sensor/Laser.hpp>

int main(int argc, char** argv) {

    ros::init(argc, argv, "Kinect2_sensor");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    std::string maps_topic, sub_topic, status_topic;
    std::string kinect2_frame;
    ph.getParam("maps_topic", maps_topic);
    ph.getParam("kinect2_laserscan", sub_topic);
    ph.getParam("kinect2_frame_id", kinect2_frame);
    ph.getParam("robot_status_topic", status_topic);

    //Topic on which kinect2 sensor has to publish
    ros::Publisher pub_obstacle = nh.advertise<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1);
    ros::ServiceClient pose_client = nh.serviceClient<dyn_utilities::getPose>("get_pose");

    sensor::Laser sensor_kinect2(nh, ph, pub_obstacle, kinect2_frame, pose_client);

    ros::Subscriber sub_laser = nh.subscribe<sensor_msgs::LaserScan>(sub_topic, 10, &sensor::Laser::laserCallback, &sensor_kinect2);
    ros::Subscriber sub_status = nh.subscribe<std_msgs::Int32>(status_topic, 1, &sensor::Laser::statusCallback, &sensor_kinect2);

    ros::AsyncSpinner spinner(0);
    spinner.start();

    ros::spin();

    return 0;
}
