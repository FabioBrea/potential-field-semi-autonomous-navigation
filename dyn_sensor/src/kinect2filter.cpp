/**
  * Class to launch a Kinect2 node
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_sensor/Kinect2.hpp>
#include <tf/transform_datatypes.h>

int main(int argc, char** argv) {

    ros::init(argc, argv, "kinect2_filter");

    ros::AsyncSpinner spinner(0);

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    //Get precomputed transformation, if available
    geometry_msgs::Transform fixed_transform;
    ph.param<double>("Translation/x", fixed_transform.translation.x, 0);
    ph.param<double>("Translation/y", fixed_transform.translation.y, 0);
    ph.param<double>("Translation/z", fixed_transform.translation.z, 0);
    ph.param<double>("Rotation/w", fixed_transform.rotation.w, 1);
    ph.param<double>("Rotation/x", fixed_transform.rotation.x, 0);
    ph.param<double>("Rotation/y", fixed_transform.rotation.y, 0);
    ph.param<double>("Rotation/z", fixed_transform.rotation.z, 0);

    ROS_INFO("Fixed Transform used:");
    ROS_INFO("Translation: (%f, %f, %f)", fixed_transform.translation.x, fixed_transform.translation.y, fixed_transform.translation.z);
    ROS_INFO("Rotation: (%f, %f, %f, %f)", fixed_transform.rotation.w, fixed_transform.rotation.x, fixed_transform.rotation.y, fixed_transform.rotation.z);

    //Get the transformation between origin and destination frame
    std::string destination_frame;
    std::string origin_frame;
    std::string sensor_topic;

    ph.getParam("destination_frame", destination_frame);
    ph.getParam("origin_frame", origin_frame);
    ph.getParam("sensor_topic", sensor_topic);

    geometry_msgs::TransformStamped tf;
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    //Wait until we got a transformation message
    while(!tfBuffer.canTransform(destination_frame, origin_frame, ros::Time(0), ros::Duration(1.0)))
    {
        ROS_WARN("Waiting for transform between origin and destination frame");
    }

    //Get the transformation
    try
    {
        tf = tfBuffer.lookupTransform(destination_frame, origin_frame, ros::Time(0));
        ROS_INFO("Transform acquired:");
        ROS_INFO("Translation: (%f, %f, %f)", tf.transform.translation.x, tf.transform.translation.y, tf.transform.translation.z);
        ROS_INFO("Rotation: (%f, %f, %f, %f)", tf.transform.rotation.w, tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z);
    }
    catch (tf2::TransformException &exception)
    {
        ROS_ERROR("Transform not retrieved");
    }

    //Apply fixed transformation to the transform between origin and destination frame
    tf.transform.translation.x -= fixed_transform.translation.x;
    tf.transform.translation.y -= fixed_transform.translation.y;
    tf.transform.translation.z += fixed_transform.translation.z;

    //Using roll, pitch, yaw to apply transformation to the rotation
    double tf_roll, tf_pitch, tf_yaw;
    double fixed_roll, fixed_pitch, fixed_yaw;

    tf::Quaternion tf_orientation;
    tf::quaternionMsgToTF(tf.transform.rotation, tf_orientation);
    tf::Matrix3x3 matrix(tf_orientation);
    matrix.getRPY(tf_roll, tf_pitch, tf_yaw);

    tf::Quaternion fixed_orientation;
    tf::quaternionMsgToTF(fixed_transform.rotation, fixed_orientation);
    tf::Matrix3x3 fixedmatrix(fixed_orientation);
    fixedmatrix.getRPY(fixed_roll, fixed_pitch, fixed_yaw);

    tf_roll += fixed_roll;
    tf_pitch += fixed_pitch;
    tf_yaw += fixed_yaw;

    //Transform back roll, pitch, yaw to quaternion
    tf::Quaternion reconvert;
    reconvert.setRPY(tf_roll, tf_pitch, tf_yaw);
    tf.transform.rotation.x = reconvert.getX();
    tf.transform.rotation.y = reconvert.getY();
    tf.transform.rotation.z = reconvert.getZ();
    tf.transform.rotation.w = reconvert.getW();

    //Topic on which the pointcloud filter has to publish
    std::string pointcloud2laserscan_topic;
    ph.getParam("pointcloud2laserscan_topic" , pointcloud2laserscan_topic);
    ros::Publisher transformation_pub = nh.advertise<sensor_msgs::PointCloud2>(pointcloud2laserscan_topic, 10);

    //Create the object handler
    sensor::Kinect2 kinect2(nh, ph, transformation_pub, tf);

    ros::Subscriber sub_pointCloud = nh.subscribe<sensor_msgs::PointCloud2>("/kinect2_head/depth_ir/points", 1, &sensor::Kinect2::filterCallback, &kinect2); ///camera/depth/points
    ros::Subscriber sub_sensor = nh.subscribe<std_msgs::Empty>(sensor_topic, 1, &sensor::Kinect2::voidCallback, &kinect2);

    spinner.start();

    ros::spin();

    return 0;
}
