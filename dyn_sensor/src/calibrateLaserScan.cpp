/**
  * This program takes images in input and calculate the transformation between them. Then it saves the transformation in a yaml file
  * @author Fabio Brea
  * @version 1.0
  */

#include <ros/ros.h>
#include <ros/package.h>
#include <tf/transform_datatypes.h>

#include <Eigen/Dense>
#include <Eigen/SVD>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <sstream>
#include <fstream>

#ifndef CALIBRATELASERSCAN_CPP
#define CALIBRATELASERSCAN_CPP

class CalcUtilities
{

private:

    ros::NodeHandle nh;
    double resolution;

private:

    /**
     * @brief sign return 1 or -1 depending from the sign of x
     * @param x: a variable to check the sign
     * @return 1 if x >= 0, -1 otherwise
     */
    double sign(double x)
    {
        return (x >= 0.0f) ? +1.0f : -1.0f;
    }

    /**
     * @brief normalizeQuaternion normalize the quaternion given in input
     * @param q: the quaternion to normalize
     * @return the normalized quaternion
     */
    tf::Quaternion normalizeQuaternion(tf::Quaternion q)
    {
        tf::Quaternion normalized_quaternion;
        double normalizing_factor = sqrt(1.0f / (q.w() * q.w() + q.x() * q.x() + q.y() * q.y() + q.z() * q.z()));
        normalized_quaternion.setW(q.w() * normalizing_factor);
        normalized_quaternion.setX(q.x() * normalizing_factor);
        normalized_quaternion.setY(q.y() * normalizing_factor);
        normalized_quaternion.setZ(q.z() * normalizing_factor);
        return normalized_quaternion;
    }

    /**
     * @brief inverseSignQuaternion changes the sign of the quaternion components.
     * @param q: the quaternion to inverse the sign
     * @return the inverse sign quaternion
     */
    tf::Quaternion inverseSignQuaternion(tf::Quaternion q)
    {
        tf::Quaternion inverse_sign_quaternion;
        inverse_sign_quaternion.setX( -q.x() );
        inverse_sign_quaternion.setY( -q.y() );
        inverse_sign_quaternion.setZ( -q.z() );
        inverse_sign_quaternion.setW( -q.w() );
        return inverse_sign_quaternion;
    }

    /**
     * @brief weightQuaternion modulate the quaternion given in input with the weight given
     * @param q: the quaternion to modulate
     * @param weight: the weight of the quaternion
     * @return the weighted quaternion
     */
    tf::Quaternion weightQuaternion(tf::Quaternion q, double weight)
    {
        tf::Quaternion weighted_quaternion;
        weighted_quaternion.setX( q.x() * weight);
        weighted_quaternion.setY( q.y() * weight);
        weighted_quaternion.setZ( q.z() * weight);
        weighted_quaternion.setW( q.w() * weight);
        return weighted_quaternion;
    }

    /**
     * @brief areQuaternionsClose controls if two quaternion are close in the space (used to check if two quaternion are similar, but with reversed sign)
     * @param q1: a quaternion
     * @param q2: a quaternion
     * @return true if q1 and q2 are close in the space, false otherwise
     */
    bool areQuaternionsClose(tf::Quaternion q1, tf::Quaternion q2)
    {
        double dotProduct = q1.x() * q2.x() + q1.y() * q2.y() + q1.z() * q2.z() + q1.w() * q2.w();
        return dotProduct >= 0.0f;
    }

public:

    /**
     * @brief CalcUtilities is the constructor for this class
     * @param nh: nodehandle for the class
     */
    CalcUtilities(ros::NodeHandle nh)
    {
        this->nh = nh;
        resolution = 0.06;
    }

    /**
     * @brief matrixIntoQuaternion transform a 3x3 Matrix in the corresponding Quaternion
     * @param rot_matrix: the matrix to transform
     * @param quat: the resulting quaternion
     */
    void matrixIntoQuaternion(Eigen::Matrix3d rot_matrix, tf::Quaternion& quat)
    {
        //Mathematical conversion of matrix in quaternion
        double w = ( rot_matrix(0,0) + rot_matrix(1,1) + rot_matrix(2,2) + 1.0f) / 4.0f;
        double x = ( rot_matrix(0,0) - rot_matrix(1,1) - rot_matrix(2,2) + 1.0f) / 4.0f;
        double y = (-rot_matrix(0,0) + rot_matrix(1,1) - rot_matrix(2,2) + 1.0f) / 4.0f;
        double z = (-rot_matrix(0,0) - rot_matrix(1,1) + rot_matrix(2,2) + 1.0f) / 4.0f;

        //Check boundaries
        if(w < 0.0f)
            w = 0.0f;
        if(x < 0.0f)
            x = 0.0f;
        if(y < 0.0f)
            y = 0.0f;
        if(z < 0.0f)
            z = 0.0f;

        w = sqrt(w);
        x = sqrt(x);
        y = sqrt(y);
        z = sqrt(z);

        if(w >= x && w >= y && w >= z)
        {
            w *= +1.0f;
            x *= sign(rot_matrix(2,1) - rot_matrix(1,2));
            y *= sign(rot_matrix(0,2) - rot_matrix(2,0));
            z *= sign(rot_matrix(1,0) - rot_matrix(0,1));
        }
        else if(x >= w && x >= y && x >= z)
        {
            w *= sign(rot_matrix(2,1) - rot_matrix(1,2));
            x *= +1.0f;
            y *= sign(rot_matrix(1,0) + rot_matrix(0,1));
            z *= sign(rot_matrix(0,2) + rot_matrix(2,0));
        }
        else if(y >= w && y >= x && y >= z)
        {
            w *= sign(rot_matrix(0,2) - rot_matrix(2,0));
            x *= sign(rot_matrix(1,0) + rot_matrix(0,1));
            y *= +1.0f;
            z *= sign(rot_matrix(2,1) + rot_matrix(1,2));
        }
        else if(z >= w && z >= x && z >= y)
        {
            w *= sign(rot_matrix(1,0) - rot_matrix(0,1));
            x *= sign(rot_matrix(2,0) + rot_matrix(0,2));
            y *= sign(rot_matrix(2,1) + rot_matrix(1,2));
            z *= +1.0f;
        }
        else
        {
            ROS_ERROR("Impossible to create quaternion from this matrix");
        }

        //Create the new quaternion
        quat.setW(w);
        quat.setX(x);
        quat.setY(y);
        quat.setZ(z);

        //Normalization, if necessary
        quat = normalizeQuaternion(quat);
    }

    /**
     * @brief averageQuaternion
     * @param quaternions: a vector that contains all the quaternion to average
     * @param weights: a vector that contains the weight of the quaternions in the vector
     * @return the averaged quaternion
     */
    tf::Quaternion averageQuaternion(std::vector<tf::Quaternion>& quaternions, std::vector<double> weights)
    {
        //Calculate total weight
        double tot_weight = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            tot_weight += *it;
        }

        Eigen::MatrixXd quaternion_matrix(4, quaternions.size());

        //Using the first quaternion of the list as reference for the sign
        tf::Quaternion firstQuaternion = quaternions.at(0);

        int i = 0;
        for(std::vector<tf::Quaternion>::iterator it = quaternions.begin(); it != quaternions.end(); ++it)
        {
            tf::Quaternion nextQuaternion = *it;
            if(!areQuaternionsClose(nextQuaternion, firstQuaternion))
            {
                //Before we add the new rotation to the average, we have to check whether the quaternion has to be inverted.
                nextQuaternion = inverseSignQuaternion(nextQuaternion);
            }

            //Using score to weight the quaternion
            double nextWeight = weights.at(i) / tot_weight;
            nextQuaternion = weightQuaternion(nextQuaternion, nextWeight);

            //Fill the corresponding cols in the matrix
            quaternion_matrix(0, i) = nextQuaternion.w();
            quaternion_matrix(1, i) = nextQuaternion.x();
            quaternion_matrix(2, i) = nextQuaternion.y();
            quaternion_matrix(3, i) = nextQuaternion.z();

            ++i;
        }

        //Compute svd. The first cols of U matrix is the averaged quaternion
        Eigen::JacobiSVD<Eigen::MatrixXd> svd(quaternion_matrix, Eigen::ComputeFullU);
        Eigen::MatrixXd EigenVectors = svd.matrixU();

        //Create the averaged quaternion
        tf::Quaternion averaged_quaternion;
        averaged_quaternion.setW(EigenVectors(0,0));
        averaged_quaternion.setX(EigenVectors(1,0));
        averaged_quaternion.setY(EigenVectors(2,0));
        averaged_quaternion.setZ(EigenVectors(3,0));

        //Invert the sign to keep rotations consistent
        if(!areQuaternionsClose(averaged_quaternion, firstQuaternion))
        {
            averaged_quaternion = inverseSignQuaternion(averaged_quaternion);
        }

        //Normalize if necessary
        averaged_quaternion = normalizeQuaternion(averaged_quaternion);

        return averaged_quaternion;
    }

    tf::Vector3 averageVector(std::vector<tf::Vector3>& vectors, std::vector<double> weights)
    {
        //Calculate total weight
        double tot_weight = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            tot_weight += *it;
        }

        //Calculate average vector
        double final_x = 0;
        double final_y = 0;
        double final_z = 0;
        int i = 0;
        for(std::vector<double>::iterator it = weights.begin(); it != weights.end(); ++it)
        {
            final_x += (vectors.at(i).x() * ((*it) / tot_weight));
            final_y += (vectors.at(i).y() * ((*it) / tot_weight));
            final_z += (vectors.at(i).z() * ((*it) / tot_weight));
            ++i;
        }

        return tf::Vector3(final_x, final_y, final_z);
    }

    /**
     * @brief pointCloudFromImage transform an image in a pointcloud. It's supposed that the background is white.
     * @param cloud: the pointcloud with the points from the image
     * @param image: the image to use as input
     * @param threshold: rgb threshold to take pixel as points
     */
    void pointCloudFromImage(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, cv::Mat& image, double threshold)
    {
        //Robot position is at the center of the image in this implementation
        int robot_x = std::floor(image.cols / 2);
        int robot_y = std::floor(image.rows / 2);

        for(int i = 0; i < image.rows; ++i)
        {
            for(int j = 0; j < image.cols; ++j)
            {
                //Takes pixel with rgb components smaller than the threshold
                if(image.at<cv::Vec3b>(i,j)[0] < threshold && image.at<cv::Vec3b>(i,j)[1] < threshold && image.at<cv::Vec3b>(i,j)[2] < threshold)
                {
                    //Transform pixel misures in meters
                    double x = (robot_x - j) * resolution;
                    double y = (robot_y - i) * resolution;

                    //Add to cloud. y is in point.z because we have different frame of reference
                    pcl::PointXYZ point;
                    point.x = x;
                    point.y = 0;
                    point.z = y;

                    cloud->push_back(point);
                }
            }
        }
    }

};


int main(int argc,char** argv)
{
    ros::init(argc, argv, "calibrate_laserscan");
    ros::NodeHandle nh;
    ros::NodeHandle ph("~");

    std::vector<tf::Vector3> linear_transform;
    std::vector<tf::Quaternion> rotation_transform;
    std::vector<double> transformation_score;

    //Create utilities object
    CalcUtilities calc(nh);

    //Parameters to load
    int num_images;
    double rgb_threshold;
    std::string target;
    std::string origin;
    std::string img_extension;

    ph.param<int>("num_images", num_images, 10);
    ph.param<double>("rgb_threshold", rgb_threshold, 250);
    ph.param<std::string>("target", target, "laser");
    ph.param<std::string>("origin", origin, "kinect");
    ph.param<std::string>("img_extension", img_extension, ".jpg");
    std::string path = ros::package::getPath("dyn_sensor") + "/images/";

    for(int k = 0; k < num_images; ++k)
    {
        std::stringstream num;
        num << k;
        cv::Mat target_image = cv::imread(path + target + num.str() + img_extension);
        cv::Mat origin_image = cv::imread(path + origin + num.str() + img_extension);

        //Transform the images in pointcloud
        pcl::PointCloud<pcl::PointXYZ>::Ptr target_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr origin_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        calc.pointCloudFromImage(target_cloud, target_image, rgb_threshold);
        calc.pointCloudFromImage(origin_cloud, origin_image, rgb_threshold);

        //Using icp to calculate the transformation between the two laserscans
        pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
        icp.setInputSource(origin_cloud);
        icp.setInputTarget(target_cloud);
        icp.setTransformationEpsilon(1e-12);
        icp.setMaxCorrespondenceDistance(0.1);
        icp.setMaximumIterations(10000);
        pcl::PointCloud<pcl::PointXYZ> result;
        icp.align(result);

        //Saving only if score is good enough
        if(icp.hasConverged() && icp.getFitnessScore() < 1)
        {
            transformation_score.push_back(1 - icp.getFitnessScore());

            //Get translation from icp final matrix
            tf::Vector3 translation(icp.getFinalTransformation()(0,3), icp.getFinalTransformation()(1,3), icp.getFinalTransformation()(2,3));
            linear_transform.push_back(translation);

            //Get rotation from icp final matrix
            Eigen::Matrix3d rotation;
            for(int i = 0; i < 3; ++i)
            {
                for(int j = 0; j < 3; ++j)
                {
                    rotation(i,j) = icp.getFinalTransformation()(i,j);
                }
            }

            //Transform matrix in a quaternion and save it
            tf::Quaternion rotation_quaternion;
            calc.matrixIntoQuaternion(rotation, rotation_quaternion);
            rotation_transform.push_back(rotation_quaternion);
        }
    }

    tf::Vector3 average_translation = calc.averageVector(linear_transform, transformation_score);
    tf::Quaternion average_rotation = calc.averageQuaternion(rotation_transform, transformation_score);

    //Return to opencv frame
    double tmp_swap = average_translation.z();
    average_translation.setZ(average_translation.y());
    average_translation.setY(tmp_swap);

    tmp_swap = average_rotation.z();
    average_rotation.setZ(average_rotation.y());
    average_rotation.setY(tmp_swap);

    //Write results in a parameter file
    std::string storage_file = ros::package::getPath("dyn_sensor") + "/params/TransformationParameters.yaml";
    cv::FileStorage fs(storage_file, cv::FileStorage::WRITE);
    fs << "Translation" << "{";
    fs << "x" << average_translation.x();
    fs << "y" << average_translation.y();
    fs << "z" << average_translation.z();
    fs << "}";

    fs << "Rotation" << "{";
    fs << "w" << average_rotation.w();
    fs << "x" << average_rotation.x();
    fs << "y" << average_rotation.y();
    fs << "z" << average_rotation.z();
    fs << "}";

    fs.release();

    //Overwrite the file, deleting the first line, because ROS is not compatible with the tag "%YAML:1.0"
    std::ifstream file;
    std::string line;
    std::string characters = "";
    file.open(storage_file.c_str());
    while(getline(file,line))
    {
        if(line!="%YAML:1.0")
        {
            characters += line + "\n";
        }
    }
    file.close();

    std::ofstream outfile;
    outfile.open(storage_file.c_str());
    outfile << characters;
    outfile.close();

    ROS_INFO("Translation: (%f, %f, %f)", average_translation.x(), average_translation.y(), average_translation.z());
    ROS_INFO("Rotation: (%f, %f, %f, %f)", average_rotation.w(), average_rotation.x(), average_rotation.y(), average_rotation.z());
    ROS_INFO("Transformation saved, you can now exit");

    return 0;
}
#endif
