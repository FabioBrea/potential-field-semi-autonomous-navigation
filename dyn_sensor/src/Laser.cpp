/**
  * Implementation of an Hokuyo class
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_sensor/Laser.hpp>

#ifndef LASER_CPP
#define LASER_CPP

namespace sensor
{

/**
  * @brief Laser: Constructor for the laser sensor
  * @param nh: the main nodehandle
  * @param ph: the private nodehandle, for getting parameters from yaml
  * @param pub: the publisher of the node
  * @param frame: the frame id of the message
  * @param pose_client: can call the service to get the current pose
  */
Laser::Laser(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, std::string frame, ros::ServiceClient pose_client)
{
    this->nh = nh;
    this->ph = ph;
    this->pub = pub;
    this->laser_frame_id = frame;
    this->pose_client = pose_client;
    robot.setStatus(PAUSED);

    ph.getParam("width", width);
    ph.getParam("height", height);
    ph.getParam("resolution", resolution);

    robot_x = std::floor(width / 2.00f);
    robot_y = std::floor(height / 2.00f);
}

/**
  * @brief getObstacleMessage: create an OccupancyGrid message, given the distances and angles from laser messages
  * @param ranges: a vector of ranges of distances
  * @return the Occupancy Grid created
  */
nav_msgs::OccupancyGrid Laser::getObstacleMessage(const std::vector<float>& ranges)
{
    std::vector<int8_t> map;

    //Initialize a map without obstacles
    for (int i = 0; i < width * height; ++i)
        map.push_back(0);

    //Use sensor message to place obstacles
    std::vector<float>::const_iterator it;
    int num_element = 0;

    for(it = ranges.begin(); it != ranges.end(); ++it)
    {
        float curr_range = *it;

        //true if curr_range is not nan and within the range
        if (!(curr_range != curr_range) && (curr_range > range_min) && (curr_range < range_max))
        {
            float curr_angle = angle_min + num_element * angle_inc;

            //Creating an obstacle in the Occupancy Grid
            int signObsX = 0;
            int signObsY = 0;

            if (std::sin(curr_angle) > 0)
                signObsX = 1;
            else if (std::sin(curr_angle) < 0)
                signObsX = -1;

            if (std::cos(curr_angle) > 0)
                signObsY = 1;
            else if (std::cos(curr_angle) < 0)
                signObsY = -1;

            int obstacle_position_x = robot_x + signObsX * std::floor(std::abs((curr_range / resolution) * std::sin(curr_angle)));
            int obstacle_position_y = robot_y + signObsY * std::floor(std::abs((curr_range / resolution) * std::cos(curr_angle)));

            //Revert to get the Occupancy Grid coherent to robot heading
            obstacle_position_x = (width - obstacle_position_x) - 1;
            obstacle_position_y = (height - obstacle_position_y) - 1;

            //Put in the map only if it's inside boundaries
            if(obstacle_position_y < height && obstacle_position_x < width && obstacle_position_y >= 0 && obstacle_position_x >= 0)
                map.at((obstacle_position_y * width) + obstacle_position_x) = 1;
        }
        num_element++;
    }

    //Creation and publishing of the Obstacle message
    nav_msgs::OccupancyGrid msg;
    nav_msgs::MapMetaData info;

    //Initialize information about the map
    info.map_load_time = ros::Time::now();
    info.resolution = resolution;
    info.width = width;
    info.height = height;

    //Initialize occupancy grid parameters
    msg.header.frame_id = laser_frame_id;
    msg.header.stamp = ros::Time::now();
    msg.info = info;
    msg.data = map;

    return msg;
}

/**
  * @brief laserCallback: gets a laser scan message and create and publish an Occupancy Grid
  * @param laser_msg: the message from laser scan
  */
void Laser::laserCallback(const sensor_msgs::LaserScan::ConstPtr& laser_msg)
{
    if (robot.getStatus() == RUNNING || robot.getStatus() == PAUSED || robot.getStatus() == READY)
    {
        //Acquisition of the message field
        this->angle_min = laser_msg->angle_min;
        this->angle_max = laser_msg->angle_max;
        this->angle_inc = laser_msg->angle_increment;
        this->range_min = laser_msg->range_min;
        this->ranges 	= laser_msg->ranges;

        if (laser_msg->range_max > width * resolution)
            this->range_max = width * resolution;
        else if (laser_msg->range_max > height * resolution)
            this->range_max = height * resolution;
        else
            this->range_max = laser_msg->range_max;

        //Creation and publishing of the Obstacle message
        nav_msgs::OccupancyGrid grid;
        grid = getObstacleMessage(ranges);

        dyn_utilities::getPose srv;
        pose_client.call(srv);

        dyn_msgs::OccupancyGridWithPose message;
        message.grid = grid;
        message.pose = srv.response.current_pose;
        pub.publish(message);
    }

    ros::spinOnce();
}

/**
  * @brief statusCallback check for change in robot state
  * @param status_msg: the state of the robot
  */
void Laser::statusCallback(const std_msgs::Int32::ConstPtr& status_msg)
{
    if(robot.hasChangedStatus(status_msg->data))
    {
        robot.setStatus(status_msg->data);
    }
}

/**
  * @brief voidCallback is needed to check if the sensor is active
  * @param empty_msg: a useless empty message
  */
void Laser::voidCallback(const std_msgs::Empty::ConstPtr& empty_msg)
{
    return;
}

}
#endif
