/**
  * Implementation of an Kinect2 class
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_sensor/Kinect2.hpp>

#ifndef KINECT2_CPP
#define KINECT2_CPP

namespace sensor
{

/**
  * @brief Kinect2: Constructor for the kinect2 sensor, initialize filters with parameters from yaml file
  * @param nh: the main nodehandle
  * @param ph: the private nodehandle, for getting parameters from yaml
  * @param pub: the publisher of the node
  * @param transform: the transformation between two frames, to get a right transformation from pointcloudtolaserscan node
  */
Kinect2::Kinect2(ros::NodeHandle nh, ros::NodeHandle ph, ros::Publisher pub, geometry_msgs::TransformStamped transform)
{
    this->nh = nh;
    this->ph = ph;
    this->pub = pub;
    this->transform = transform;

    ph.getParam("kinect2_frame_id", kinect2_frame_id);
    ph.getParam("destination_frame", destination_frame);

    //Initialization of PassThrough filter, using ConditionAnd for multiple condition
    pcl::ConditionAnd<pcl::PointXYZ>::Ptr filter_condition (new pcl::ConditionAnd<pcl::PointXYZ>());
    double x_min_tresh;
    double x_max_tresh;
    double y_min_tresh;
    double y_max_tresh;
    double z_min_tresh;
    double z_max_tresh;
    ph.getParam("x_min_tresh", x_min_tresh);
    ph.getParam("x_max_tresh", x_max_tresh);
    ph.getParam("y_min_tresh", y_min_tresh);
    ph.getParam("y_max_tresh", y_max_tresh);
    ph.getParam("z_min_tresh", z_min_tresh);
    ph.getParam("z_max_tresh", z_max_tresh);

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, x_max_tresh)));

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, x_min_tresh)));

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT, y_max_tresh)));

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, y_min_tresh)));

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT, z_max_tresh)));

    filter_condition->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
                                                                                    pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, z_min_tresh)));

    cond_rem = new pcl::ConditionalRemoval<pcl::PointXYZ>();
    cond_rem->setCondition(filter_condition);
    cond_rem->setKeepOrganized(true);

    //Initialization of voxel filter
    double leaf_size;
    ph.getParam("leaf_size", leaf_size);

    vox.setLeafSize(leaf_size, leaf_size, leaf_size);

    //Initialization of passthrough filter to splits the pointcloud
    double split_distance;
    ph.getParam("split_distance", split_distance);
    pass_thr.setFilterFieldName ("z");
    pass_thr.setFilterLimits (0.0, split_distance);

    //Initialization of Statistical Outlier Removal filter
    int mean_k;
    ph.getParam("mean_k", mean_k);
    ph.getParam("near_std_dev_mul_thresh", near_std_dev);
    ph.getParam("far_std_dev_mul_thresh", far_std_dev);

    sor.setMeanK(mean_k);

    //Initialization of Radius Outlier Removal
    double rad_search;
    int num_neighbours;
    ph.getParam("near_rad_search", rad_search);
    ph.getParam("near_num_neighbours", num_neighbours);

    ror.setRadiusSearch(rad_search);
    ror.setMinNeighborsInRadius(num_neighbours);
}

/**
  * @brief filterCallback: gets a pointcloud message and filter it, then publish the resulting pointcloud message
  * @param point_cloud_msg: the message from kinect2
  */
void Kinect2::filterCallback(const sensor_msgs::PointCloud2::ConstPtr& point_cloud_msg)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(*point_cloud_msg, *cloud);

    //Using Passthrough filter
    pcl::PointCloud<pcl::PointXYZ>::Ptr condAnd_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    cond_rem->setInputCloud(cloud);
    cond_rem->filter (*condAnd_cloud);

    //Using Voxel filter
    pcl::PointCloud<pcl::PointXYZ>::Ptr vox_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    vox.setInputCloud(condAnd_cloud);
    vox.filter(*vox_cloud);

    //Split in two the pointcloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr near_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr far_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    pass_thr.setInputCloud(vox_cloud);
    pass_thr.filter (*near_cloud);
    pass_thr.setFilterLimitsNegative(true);
    pass_thr.filter (*far_cloud);

    //Remove blocks of noise in the near cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr near_ror_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    if (near_cloud->size() > 0)
    {
        ror.setInputCloud(near_cloud);
        ror.filter(*near_ror_cloud);
    }
    else
        *near_ror_cloud = *near_cloud;


    //Using statistical outlier removal
    pcl::PointCloud<pcl::PointXYZ>::Ptr near_sor_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr far_sor_cloud(new pcl::PointCloud<pcl::PointXYZ>);

    //Strong filtering can remove all points
    if (near_ror_cloud->size() > 0)
    {
        sor.setInputCloud(near_ror_cloud);
        sor.setStddevMulThresh(near_std_dev);
        sor.filter(*near_sor_cloud);
    }
    else
        *near_sor_cloud = *near_ror_cloud;

    if (far_cloud->size() > 0)
    {
        sor.setInputCloud(far_cloud);
        sor.setStddevMulThresh(far_std_dev);
        sor.filter(*far_sor_cloud);
    }
    else
        *near_sor_cloud = *far_cloud;

    near_sor_cloud->operator +=(*far_sor_cloud);

    //Create a pointcloud message and transform into the right frame
    sensor_msgs::PointCloud2 message, laser_msg;
    message.header = point_cloud_msg->header;
    pcl::toROSMsg(*near_sor_cloud, message);

    tf2::doTransform (message, laser_msg, transform);
    laser_msg.header = point_cloud_msg->header;
    laser_msg.header.frame_id = destination_frame;

    //Publish pointcloud message
    pub.publish(laser_msg);
}

/**
  * @brief voidCallback is needed to check if the sensor is active
  * @param empty_msg: a useless empty message
  */
void Kinect2::voidCallback(const std_msgs::Empty::ConstPtr& empty_msg)
{
    return;
}
}
#endif
