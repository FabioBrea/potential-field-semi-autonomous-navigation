/**
  * Class to launch a Laser node
  * @author Fabio Brea
  * @version 1.0
  */

#include <dyn_sensor/Laser.hpp>

int main(int argc, char** argv) {

    ros::init(argc, argv, "laser_sensor");

    //Main nodehandle
    ros::NodeHandle nh;

    //Private nodehandle
    ros::NodeHandle ph("~");

    std::string maps_topic, sensor_topic, laser_topic, status_topic;
    std::string frame;
    ph.getParam("maps_topic" , maps_topic);
    ph.getParam("sensor_topic", sensor_topic);
    ph.getParam("laser_frame_id", frame);
    ph.getParam("laser_topic", laser_topic);
    ph.getParam("robot_status_topic", status_topic);

    //Topic on which the laser sensor has to publish
    ros::Publisher pub_map = nh.advertise<dyn_msgs::OccupancyGridWithPose>(maps_topic, 1);
    ros::ServiceClient pose_client = nh.serviceClient<dyn_utilities::getPose>("get_pose");

    sensor::Laser sensor_laser(nh, ph, pub_map, frame, pose_client);

    ros::Subscriber sub_laser = nh.subscribe<sensor_msgs::LaserScan>(laser_topic, 1, &sensor::Laser::laserCallback, &sensor_laser);
    ros::Subscriber sub_status = nh.subscribe<std_msgs::Int32>(status_topic, 1, &sensor::Laser::statusCallback, &sensor_laser);
    ros::Subscriber sub_sensor = nh.subscribe<std_msgs::Empty>(sensor_topic, 1, &sensor::Laser::voidCallback , &sensor_laser);

    ros::spin();

    return 0;
}
